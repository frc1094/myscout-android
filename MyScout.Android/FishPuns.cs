﻿using System;
using System.Text;
using System.IO;

namespace MyScout.Android
{
    public static class FishPuns
    {
        // Variables/Constants
        public const string FilePath = "Fish.puns";
        public const uint Signature = 0x4E5550; // "PUN" in little-endian ASCII
        public const byte Version = 1;
        private static Random rand;

        // Methods
        public static string GetRandomFishPun(Stream fs)
        {
            using (var reader = new BinaryReader(fs, Encoding.UTF8))
            {
                // Signature Check
                uint header = reader.ReadUInt32();
                if ((header & 0xFFFFFF) != Signature)
                {
                    return null;
                }

                // Version Check
                byte ver = (byte)((header & 0xFF000000) >> 24);
                if (ver > Version)
                {
                    return null;
                }

                // Pick a random pun to read
                uint punsCount = reader.ReadUInt32();
                if (rand == null)
                    rand = new Random();

                // Jump to the position of the pun
                int fishPun = rand.Next((int)punsCount);
                fs.Seek(fishPun * 4, SeekOrigin.Current);

                uint offset = reader.ReadUInt32();
                fs.Seek(offset, SeekOrigin.Begin);

                // Read the pun
                return reader.ReadString();
            }
        }
    }
}