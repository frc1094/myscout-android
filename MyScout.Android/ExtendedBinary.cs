﻿using System;
using System.Text;
using System.IO;
using Android.Runtime;

namespace MyScout.Android
{
    public class ExtendedBinaryReader : BinaryReader
    {
        // Constructors
        public ExtendedBinaryReader(Stream input, bool leaveOpen = false) :
            base(input, Encoding.UTF8, leaveOpen) { }

        public ExtendedBinaryReader(Stream input, Encoding encoding,
            bool leaveOpen = false) : base(input, encoding, leaveOpen) { }

        // Methods
        public virtual string ReadSignature(int length = 3)
        {
            var sig = new char[length];
            Read(sig, 0, length);
            return new string(sig);
        }

        public virtual Team ReadTeam()
        {
            string name = ReadString();
            string id = ReadString();
            return new Team(name, id);
        }

        public virtual Time ReadTime()
        {
            long start = ReadInt64();
            long stop = ReadInt64();

            var startTime = DateTime.FromBinary(start);
            var stopTime = DateTime.FromBinary(stop);
            return new Time(startTime, stopTime);
        }

        public virtual byte[] ReadInChunks(int length, int chunkSize)
        {
            return IO.ReadInChunks(BaseStream, length, chunkSize);
        }

        public virtual object ReadByType(Type type)
        {
            // Only supports types that DataPoints support
            if (type == typeof(bool))
            {
                return ReadBoolean();
            }
            else if (type == typeof(int))
            {
                return ReadInt32();
            }
            else if (type == typeof(float))
            {
                return ReadSingle();
            }
            else if (type == typeof(double))
            {
                return ReadDouble();
            }
            else if (type == typeof(string))
            {
                return ReadString();
            }
            else if (type == typeof(Time))
            {
                return ReadTime();
            }
            else if (type == typeof(JavaList<Time>))
            {
                var times = new JavaList<Time>();
                int count = ReadInt32();

                for (int i = 0; i < count; ++i)
                {
                    times.Add(ReadTime());
                }

                return times;
            }

            return null;
        }
    }

    public class ExtendedBinaryWriter : BinaryWriter
    {
        // Constructors
        public ExtendedBinaryWriter(Stream output) : base(output, Encoding.UTF8, false) { }
        public ExtendedBinaryWriter(Stream output, Encoding encoding,
            bool leaveOpen = false) : base(output, encoding, leaveOpen) { }

        // Methods
        public virtual void WriteSignature(string sig)
        {
            Write(sig.ToCharArray());
        }

        public virtual void Write(Team team)
        {
            Write(team.Name);
            Write(team.ID);
        }

        public virtual void Write(Time time)
        {
            if (time.IsTiming)
                time.StopTiming();

            Write(time.StartTime.ToBinary());
            Write(time.StopTime.ToBinary());
        }

        public virtual void WriteInChunks(byte[] data, int chunkSize)
        {
            IO.WriteInChunks(BaseStream, data, chunkSize);
        }

        public virtual void WriteByType(object data, Type type)
        {
            // Only supports types that DataPoints support
            if (type == typeof(bool))
            {
                Write((bool)data);
            }
            else if (type == typeof(int))
            {
                Write((int)data);
            }
            else if (type == typeof(float))
            {
                Write((float)data);
            }
            else if (type == typeof(double))
            {
                Write((double)data);
            }
            else if (type == typeof(string))
            {
                Write((string)data);
            }
            else if (type == typeof(Time))
            {
                Write((Time)data);
            }
            else if (type == typeof(JavaList<Time>))
            {
                var times = (data as JavaList<Time>);
                Write(times.Count);

                foreach (var time in times)
                {
                    Write(time);
                }
            }
        }
    }
}