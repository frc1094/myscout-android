﻿using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Widget;
using System;

namespace MyScout.Android.UI
{
    [Activity(Label = "Edit Event", Icon = "@drawable/icon",
        Theme = "@style/MyScoutTheme")]
    public class EditEventActivity : ToolbarActivity
    {
        // Variables/Constants
        protected EditText eventNameTxtbx;
        protected Button eventDataSetBtn, okBtn;
        protected int eventIndex;

        protected const int GET_DATASET_REQUEST = 0;

        // Methods
        protected void RefreshGUI()
        {
            // Yeah I know this is kind of a bad way of checking this lol
            okBtn.Enabled = (eventDataSetBtn.Text != "- No DataSet Assigned -");
        }

        // GUI Events
        protected override void OnCreate()
        {
            // Setup GUI
            SetContentView(Resource.Layout.EditEventLayout);

            // Assign local references to GUI elements
            eventNameTxtbx = FindViewById<EditText>(Resource.Id.EditEventNameTxtbx);
            eventDataSetBtn = FindViewById<Button>(Resource.Id.EditEventDataSetBtn);
            okBtn = FindViewById<Button>(Resource.Id.EditEventBtn);

            // Assign events to GUI elements
            eventDataSetBtn.Click += EventDataSetBtn_Click;
            okBtn.Click += OkBtn_Click;

            // Get event index
            eventIndex = Intent.GetIntExtra("ItemIndex", -1);

            // If an event index was assigned, allow the user to edit that event
            if (eventIndex >= 0)
            {
                var evnt = EventActivity.Events[eventIndex];
                eventNameTxtbx.Text = evnt.Name;
                eventDataSetBtn.Text = evnt.DataSetFileName;
                eventDataSetBtn.Enabled = false;
            }

            // If no event index was assigned, we must be adding an event instead
            else
            {
                Title = "Create Event";
            }

            RefreshGUI();
        }

        protected override void OnActivityResult(
            int requestCode, [GeneratedEnum]Result resultCode, Intent data)
        {
            if (requestCode == GET_DATASET_REQUEST)
            {
                if (resultCode == Result.Ok)
                {
                    // Set the selected DataSet using the index returned from the DataSet activity
                    int selectedDataSetIndex = data.GetIntExtra("SelectedItemIndex", -1);
                    if (selectedDataSetIndex < 0 || DataSetActivity.DataSets == null)
                        return;

                    var dataSet = DataSetActivity.DataSets[selectedDataSetIndex];
                    eventDataSetBtn.Text = $"{dataSet}{DataSet.Extension}";
                }

                RefreshGUI();
            }
        }

        protected void EventDataSetBtn_Click(object sender, EventArgs e)
        {
            // Bring up DataSet list
            StartActivityForResult(
                typeof(DataSetActivity), GET_DATASET_REQUEST);
        }

        protected void OkBtn_Click(object sender, EventArgs e)
        {
            // Apply our changes and close the dialog
            if (eventIndex >= 0)
            {
                // Edit existing event data
                var evnt = EventActivity.Events[eventIndex];
                evnt.Name = eventNameTxtbx.Text;
                evnt.DataSetFileName = eventDataSetBtn.Text;
            }
            else
            {
                // Generate a new event
                var evnt = new Event()
                {
                    Name = eventNameTxtbx.Text,
                    DataSetFileName = eventDataSetBtn.Text,
                    CurrentRoundIndex = 0
                };

                evnt.Rounds.Add(new Round());

                // Save the event and add it as a new entry to the list
                evnt.Save();
                EventActivity.Events.Add(evnt);
            }

            Finish();
        }
    }
}