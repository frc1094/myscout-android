﻿using Android.App;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using System.Collections.Generic;

using AppCompatTextView = Android.Support.V7.Widget.AppCompatTextView;

namespace MyScout.Android.UI
{
    [Activity(Label = "Round", Icon = "@drawable/icon",
        Theme = "@style/MyScoutTheme")]
    public class RoundActivity : ToolbarActivity
    {
        // Variables/Constants
        public static RoundActivity Instance;
        public static Team CurrentTeam;
        public static int TeamSlotIndex = -1;
        public static bool PreScouting = false;

        public object[] AutoData
        {
            get
            {
                return GetGUIData(DataSet.Current.RoundAutoData, autoLayout);
            }

            set
            {
                SetGUIData(DataSet.Current.RoundAutoData, autoLayout, value);
            }
        }

        public object[] TeleOPData
        {
            get
            {
                return GetGUIData(DataSet.Current.RoundTeleOPData, teleOPLayout);
            }

            set
            {
                SetGUIData(DataSet.Current.RoundTeleOPData, teleOPLayout, value);
            }
        }

        public object[] PreScoutingData
        {
            get
            {
                return GetGUIData(DataSet.Current.PreScoutingData, autoLayout);
            }

            set
            {
                SetGUIData(DataSet.Current.PreScoutingData, autoLayout, value);
            }
        }

        protected LinearLayout autoLayout, teleOPLayout;
        protected TextView autoText, teleOPText;

        // Methods
        protected object[] GetGUIData(List<DataPoint> dataPoints, LinearLayout layout)
        {
            var data = new object[dataPoints.Count];
            int i = -1;

            for (int layoutIndex = 0; layoutIndex < layout.ChildCount; ++layoutIndex)
            {
                var view = layout.GetChildAt(layoutIndex);
                if (view.GetType() == typeof(TextView) ||
                    view.GetType() == typeof(AppCompatTextView))
                {
                    continue;
                }

                // Get the appropriate data based on type
                var dataPoint = dataPoints[++i];
                var type = dataPoint.DataType;

                if (dataPoint.Choices.Count > 0)
                {
                    // Choices
                    var group = (view as RadioGroup);
                    var rb = group.FindViewById<RadioButton>(
                        group.CheckedRadioButtonId);

                    var choice = (rb.Tag as DataPoint.Choice);
                    data[i] = choice.Object;
                }
                else if (type == typeof(bool))
                {
                    // Booleans
                    var chkBx = (view as CheckBox);
                    data[i] = (chkBx == null) ? false : chkBx.Checked;
                }
                else if (type == typeof(string))
                {
                    // Strings
                    var txtBx = (view as EditText);
                    data[i] = txtBx?.Text;
                }
                else if (type == typeof(int))
                {
                    // Integers
                    if (view is RelativeLayout l)
                    {
                        var txt = (l.GetChildAt(1) as TextView);
                        if (txt != null)
                        {
                            data[i] = (int.TryParse(txt.Text, out int v)) ? v : 0;
                            continue;
                        }
                    }

                    data[i] = 0;
                }
                else if (type == typeof(Time) || type == typeof(JavaList<Time>))
                {
                    // Times
                    if (view is RelativeLayout l)
                    {
                        var btn = (l.GetChildAt(0) as Button);
                        if (btn != null && btn.Tag != null)
                        {
                            var time = (btn.Tag as Time);
                            if (time == null)
                            {
                                var times = (btn.Tag as JavaList<Time>);
                                if (times == null)
                                    continue;

                                if (times.Count > 0)
                                {
                                    time = times[times.Count - 1];
                                    time.StopTiming();
                                }

                                data[i] = times;
                                continue;
                            }

                            time.StopTiming();
                            data[i] = time;
                            continue;
                        }
                    }

                    data[i] = null;
                }
                else
                {
                    // Decimal Numbers
                    if (view is EditText txtBx && !string.IsNullOrEmpty(txtBx.Text))
                    {
                        if (type == typeof(float))
                        {
                            data[i] = (float.TryParse(txtBx.Text, out float d)) ? d : 0f;
                            continue;
                        }
                        else if (type == typeof(double))
                        {
                            data[i] = (double.TryParse(txtBx.Text, out double d)) ? d : 0d;
                            continue;
                        }
                    }

                    data[i] = 0;
                }
            }

            return data;
        }

        protected void SetGUIData(List<DataPoint> dataPoints,
            LinearLayout layout, object[] data)
        {
            if (data == null)
                return;

            int i = -1;
            for (int layoutIndex = 0; layoutIndex < layout.ChildCount; ++layoutIndex)
            {
                var view = layout.GetChildAt(layoutIndex);
                if (view.GetType() == typeof(TextView) ||
                    view.GetType() == typeof(AppCompatTextView))
                {
                    continue;
                }

                // Get the appropriate data based on type
                var dataPoint = dataPoints[++i];
                var type = dataPoint.DataType;

                if (dataPoint.Choices.Count > 0)
                {
                    // Choices
                    if (view is RadioGroup group)
                    {
                        // Get Choice Value
                        DataPoint.Choice c = null;
                        foreach (var choice in dataPoint.Choices)
                        {
                            if (choice.Value.Object.Equals(data[i]))
                            {
                                c = choice.Value;
                                break;
                            }
                        }

                        if (c == null)
                            continue;

                        // Find the corresponding RadioButton and check it
                        for (int i2 = 0; i2 < group.ChildCount; ++i2)
                        {
                            var rb = (group.GetChildAt(i2) as RadioButton);
                            if (rb == null)
                                continue;

                            if (rb.Tag == c)
                            {
                                group.Check(rb.Id);
                                break;
                            }
                        }
                    }
                }
                else if (type == typeof(bool))
                {
                    // Booleans
                    if (view is CheckBox chkBx)
                        chkBx.Checked = (bool)data[i];
                }
                else if (type == typeof(string))
                {
                    // Strings
                    if (view is EditText txtBx)
                        txtBx.Text = (string)data[i];
                }
                else if (type == typeof(int))
                {
                    // Integers
                    if (view is RelativeLayout l)
                    {
                        var txt = (l.GetChildAt(1) as TextView);
                        if (txt != null)
                            txt.Text = data[i].ToString();
                    }
                }
                else if (type == typeof(Time) || type == typeof(JavaList<Time>))
                {
                    // Times
                    if (view is RelativeLayout l)
                    {
                        var btn = (l.GetChildAt(0) as Button);
                        if (btn != null)
                        {
                            if (type == typeof(JavaList<Time>))
                            {
                                var times = (data[i] as JavaList<Time>);
                                if (times == null)
                                    continue;

                                btn.Tag = times;
                                btn.Text = (times.Count > 0) ?
                                    $"Start New Timer ({times.Count})" :
                                    "Start Timer";
                            }
                            else
                            {
                                var time = (data[i] as Time);
                                btn.Tag = time;
                                btn.Text = (time != null) ?
                                    $"Reset Timer ({time.Duration.Seconds} Seconds)" :
                                    "Start Timer";
                            }
                        }
                    }
                }
                else
                {
                    // Decimal Numbers
                    if (view is EditText txtBx)
                        txtBx.Text = data[i].ToString();
                }
            }
        }

        // GUI Events
        protected override void OnCreate()
        {
            // Setup GUI
            Instance = this;
            SetContentView(Resource.Layout.RoundLayout);

            // Assign local references to GUI elements
            autoText = FindViewById<TextView>(Resource.Id.AutoTextView);
            teleOPText = FindViewById<TextView>(Resource.Id.TeleOPTextView);
            autoLayout = FindViewById<LinearLayout>(Resource.Id.AutoLinearLayout);
            teleOPLayout = FindViewById<LinearLayout>(Resource.Id.TeleOPLinearLayout);

            // Set ActionBar Label
            if (CurrentTeam != null)
            {
                Title = CurrentTeam.ToString();
            }

            // Build GUI from DataSet
            if (DataSet.Current != null)
            {
                if (PreScouting)
                {
                    // Build Pre-Scouting GUI
                    DataSet.Current.FillPreScoutingGUI(autoLayout);
                    autoText.Text = "Pre-Scouting Data";
                    teleOPText.Text = "";
                }
                else
                {
                    // Build Autonomous GUI
                    DataSet.Current.FillAutonomousGUI(autoLayout);
                    if (autoLayout.ChildCount < 1)
                    {
                        autoText.Text = "";
                    }

                    // Build Tele-OP GUI
                    DataSet.Current.FillTeleOPGUI(teleOPLayout);
                    if (teleOPLayout.ChildCount < 1)
                    {
                        teleOPText.Text = "";
                    }
                }
            }

            // If Scout Master, set GUI element values
            if (Config.TabletType == Config.TabletTypes.ScoutMaster &&
                TeamSlotIndex >= 0)
            {
                var currentRound = Event.Current.CurrentRound;
                var teamData = currentRound.TeamData[TeamSlotIndex];

                AutoData = teamData.AutoData;
                TeleOPData = teamData.TeleOPData;
            }

            // If Pre-Scouting, set GUI element values
            if (PreScouting && CurrentTeam.PreScoutingData != null)
            {
                PreScoutingData = CurrentTeam.PreScoutingData;
            }
        }

        public override void OnBackPressed()
        {
            Log.Debug("MyScout", "back pressed");
            // Update Event Auto/Tele-OP Data if Scout Master
            if (Config.TabletType == Config.TabletTypes.ScoutMaster &&
                TeamSlotIndex >= 0)
            {
                var currentRound = Event.Current.CurrentRound;
                var teamData = currentRound.TeamData[TeamSlotIndex];

                teamData.AutoData = AutoData;
                teamData.TeleOPData = TeleOPData;

                Event.Current.Save();
            }

            // Update Pre-Scouting Data if Pre-Scouting
            if (PreScouting)
            {
                Log.Debug("MyScout", "saving");
                CurrentTeam.PreScoutingData = PreScoutingData;
                Event.Current.Save();
                Log.Debug("MyScout", "saved");
            }

            // Finish this activity
            PreScouting = false;
            TeamSlotIndex = -1;
            CurrentTeam = null;

            Finish();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
    }
}