﻿using System.Collections.Generic;
using Android.App;

namespace MyScout.Android.UI
{
    [Activity(Label = "Spreadsheets", Icon = "@drawable/icon",
        Theme = "@style/MyScoutTheme")]
    public class RoundDatasheetFilterSelectActivity : ListActivity<string>
    {
        protected override void OnCreate()
        {
            base.OnCreate();

            Event currEvent = Event.Current;

            List<string> roundlist = new List<string>();
            roundlist.Add("Show All Teams");

            int[] teamRounds = SpreadsheetHelper.FindAllTeamRounds();
            for(int i = 0; i < teamRounds.Length; ++i)
            {
                roundlist.Add($"Round {teamRounds[i]} - {GetRoundDetails(currEvent.Rounds[teamRounds[i]].TeamData)}");
            }
            
            SetupList(roundlist, typeof(string));
        }

        string GetRoundDetails(TeamData[] teamData)
        {
            string result = "";

            for(int i = 0; i < teamData.Length; ++i)
            {
                result += $"{teamData[i].Team.ID}, ";
            }

            return result.Substring(0, result.Length - 2);
        }
    }
}