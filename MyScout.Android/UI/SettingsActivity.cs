﻿using Android.App;
using Android.Views;
using Android.Widget;
using System;

namespace MyScout.Android.UI
{
    [Activity(Label = "Settings", Icon = "@drawable/icon",
        Theme = "@style/MyScoutTheme")]
    public class SettingsActivity : ToolbarActivity
    {
        // Variables/Constants
        public static bool IsFirstTimeSetup = false;
        protected LinearLayout typeLayout, devicesLayout;
        protected RadioGroup typeGroup, devicesGroup;
        protected RadioButton scoutOption, scoutMasterOption;
        protected Button refreshDevicesBtn, okBtn;
        protected string scoutMasterAddress;

        // Methods
        protected string GetSelectedDevice()
        {
            if (devicesGroup.CheckedRadioButtonId < 0) return null;

            // Get selected RadioButton
            var rb = devicesGroup.FindViewById<RadioButton>
                (devicesGroup.CheckedRadioButtonId);

            // Return device address
            return (string)rb.Tag;
        }

        protected void UpdateUI(bool refreshDevices = true)
        {
            devicesLayout.Visibility =
                (scoutOption.Checked) ?
                ViewStates.Visible : ViewStates.Gone;

            okBtn.Enabled = (scoutMasterOption.Checked ||
                !string.IsNullOrEmpty(scoutMasterAddress));

            if (refreshDevices && scoutOption.Checked)
                RefreshDevices();
        }

        protected void RefreshDevices()
        {
            // Prepare the Bluetooth adapter
            if (!BluetoothIO.PrepareAdapter())
            {
                // Show toast error message if the device doesn't have a Bluetooth adapter
                Toast.MakeText(this, BluetoothIO.NoAdapterMessage, ToastLength.Long);
                return;
            }

            // Update list of paired devices
            var adapter = BluetoothIO.Adapter;
            adapter.StartDiscovery();
            devicesGroup.RemoveAllViews();

            int id = -1;
            foreach (var d in adapter.BondedDevices)
            {
                var rb = new RadioButton(this)
                {
                    Text = $"{d.Name} ({d.Address})",
                    Tag = d.Address,
                };

                devicesGroup.AddView(rb);

                if (d.Address == scoutMasterAddress)
                    id = rb.Id;
            }

            if (id >= 0)
                devicesGroup.Check(id);

            // Cancel device discovery as it slows down a Bluetooth connection
            adapter.CancelDiscovery();
        }

        // GUI Events
        protected override void OnCreate()
        {
            // Setup GUI
            SetContentView(Resource.Layout.SettingsLayout);

            // Assign local references to GUI elements
            typeLayout = FindViewById<LinearLayout>(Resource.Id.SettingsTypeLayout);
            typeGroup = FindViewById<RadioGroup>(Resource.Id.SettingsTypeGroup);
            scoutOption = FindViewById<RadioButton>(Resource.Id.SettingsScoutOption);
            scoutMasterOption = FindViewById<RadioButton>(Resource.Id.SettingsScoutMasterOption);

            devicesLayout = FindViewById<LinearLayout>(Resource.Id.SettingsDevicesLayout);
            devicesGroup = FindViewById<RadioGroup>(Resource.Id.BluetoothDevicesGroup);
            refreshDevicesBtn = FindViewById<Button>(Resource.Id.RefreshDevicesBtn);

            okBtn = FindViewById<Button>(Resource.Id.SettingsOKBtn);

            // Assign events to GUI elements
            typeGroup.CheckedChange += TypeGroup_CheckedChange;
            devicesGroup.CheckedChange += DevicesGroup_CheckedChange;
            refreshDevicesBtn.Click += RefreshBtn_Click;
            okBtn.Click += OkBtn_Click;

            // Change GUI elements to match settings
            if (IsFirstTimeSetup)
            {
                Title = "First-Time Setup";
            }

            // Load config file if not already loaded
            else if (!Config.Loaded)
            {
                Config.Load();
            }

            scoutOption.Checked = (Config.TabletType == Config.TabletTypes.Scout);
            scoutMasterOption.Checked = (Config.TabletType == Config.TabletTypes.ScoutMaster);
            scoutMasterAddress = Config.ScoutMasterAddress;
            UpdateUI();
        }

        private void TypeGroup_CheckedChange(
            object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            UpdateUI();
        }

        private void DevicesGroup_CheckedChange(
            object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            scoutMasterAddress = (e.CheckedId < 0) ? null :
                (string)devicesGroup.FindViewById<RadioButton>(e.CheckedId).Tag;

            UpdateUI(false);
        }

        private void RefreshBtn_Click(object sender, EventArgs e)
        {
            RefreshDevices();
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            // Change setings to match GUI elements
            bool isScoutMaster = scoutMasterOption.Checked;
            Config.TabletType = (isScoutMaster) ?
                Config.TabletTypes.ScoutMaster : Config.TabletTypes.Scout;

            // Register devices
            Config.ScoutMasterAddress = (isScoutMaster) ?
                null : scoutMasterAddress;

            // Save config file
            Config.Save();

            // Close settings activity
            if (IsFirstTimeSetup)
            {
                IsFirstTimeSetup = false;
                StartActivity(typeof(MainActivity));
            }

            Finish();

            // TODO: Fix UI not changing if you change tablet type, then exit settings.
        }
    }
}