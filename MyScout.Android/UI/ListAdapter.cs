﻿using Android.Support.V7.Widget;
using Android.Views;
using System.Collections.Generic;
using System;
using System.IO;

namespace MyScout.Android.UI
{
    public class ListAdapter<T> : RecyclerView.Adapter
    {
        // Variables/Constants
        public Type EditItemActivity { get; protected set; }
        public List<T> List;
        public ListActivity<T> Activity => activity;
        protected List<T> originalList;

        public override int ItemCount
        {
            get => (List == null) ?
                0 : List.Count;
        }

        protected ListActivity<T> activity;
        
        // Constructors
        public ListAdapter(ListActivity<T> activity,
            List<T> data, Type editItemActivity)
        {
            this.activity = activity;
            EditItemActivity = editItemActivity;
            List = data;
            originalList = data;
        }

        // Methods
        public void Add(T item)
        {
            originalList.Add(item);
            if (List == originalList)
            {
                NotifyItemInserted(originalList.Count - 1);
            }
            else
            {
                // TODO: Clear text in search bar?
                ClearFilter();
            }
        }

        public void Remove(ListEntryViewHolder<T> item)
        {
            // Close the popup menu if it happens to be open
            item.ClosePopupMenu();

            // Delete DataSet
            if (typeof(T) == typeof(string))
            {
                string filePath = Path.Combine(IO.DataSetDirectory,
                    $"{item.Label.Text}{DataSet.Extension}");

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
            }

            // Remove the item from the list
            Remove(item.AdapterPosition);
        }

        public void Remove(int index)
        {
            // Remove the item at the given postion from the list and tell
            // Android we did (so Android removes that item from the UI list, too).
            int filteredIndex = index;
            index = GetUnFilteredPosition(index);

            // Remove the item from the list
            // TODO: Move type-specific stuff to their respective activities

            // Update team indices
            if (typeof(T) == typeof(Team))
            {
                if (Event.Current == null)
                    return;

                TeamData teamData;
                foreach (var round in Event.Current.Rounds)
                {
                    for (int i = 0; i < Round.TeamCount; ++i)
                    {
                        teamData = round.TeamData[i];
                        if (teamData == null || teamData.TeamIndex < index)
                            continue;

                        if (teamData.TeamIndex == index)
                        {
                            // If this team slot references the
                            // team being deleted, make the team null.
                            round.TeamData[i] = null;
                        }
                        else
                        {
                            // If this team slot references a team with an index
                            // greater than that of the team being deleted, subtract
                            // one from the slot's index to make sure the slot still
                            // points to the correct team despite the team list shrinking.
                            --teamData.TeamIndex;
                        }
                    }
                }
            }

            // Delete Event
            else if (typeof(T) == typeof(Event))
            {
                var evnt = EventActivity.Events[index];
                string filePath = Path.Combine(Event.EventsDirectory,
                    $"{Event.FileNamePrefix}{evnt.Index}{Event.Extension}");

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
            }

            originalList.RemoveAt(index);
            if (List != originalList)
                List.RemoveAt(filteredIndex);

            NotifyItemRemoved(filteredIndex);
        }

        public void ClearFilter()
        {
            List = originalList;
            NotifyDataSetChanged();
        }

        public void SetFilter(string filter)
        {
            if (string.IsNullOrEmpty(filter))
            {
                ClearFilter();
                return;
            }

            // Clear the list
            List = new List<T>();

            // Add only items that match the filter
            string str;
            filter = filter.ToLower();

            for (int i = 0; i < originalList.Count; ++i)
            {
                str = originalList[i].ToString().ToLower();
                if (str.Contains(filter))
                    List.Add(originalList[i]);
            }

            NotifyDataSetChanged();
        }

        public int GetUnFilteredPosition(int index)
        {
            // Returns the position of the item in the original, unfiltered list.
            if (List == originalList)
                return index;

            return originalList.IndexOf(List[index]);
        }

        // GUI Events
        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            // Create a new ListEntryViewHolder from the UI layout
            // we made defined in ListEntryLayout.axml
            var itemView = LayoutInflater.From(parent.Context).Inflate(
                Resource.Layout.ListEntryLayout, parent, false);

            return new ListEntryViewHolder<T>(itemView, activity, this);
        }
        
        public override void OnBindViewHolder(
            RecyclerView.ViewHolder holder, int position)
        {
            // Update the text on the ListEntryViewHolder's TextView object to
            // match the list item that should be displayed at this position.
            var tvh = (holder as ListEntryViewHolder<T>);
            if (tvh == null)
                return;

            var item = List[position];
            tvh.Label.Text = GetTextForListItem(item);
        }

        protected virtual string GetTextForListItem(object item)
        {
            // Made a method so it can be easily overridden
            return item.ToString();
        }
    }
}