﻿using System;
using Android.Text;
using Java.Lang;

namespace MyScout.Android.UI
{
    public class NumericTypeRangeFilter : Java.Lang.Object, IInputFilter
    {
        // Variables/Constants
        public Type NumberType;

        // Constructors
        public NumericTypeRangeFilter(Type type)
        {
            NumberType = type;
        }

        // Methods
        public ICharSequence FilterFormatted(ICharSequence source, int start,
            int end, ISpanned dest, int dstart, int dend)
        {
            // Get what the user is trying to type in
            string text = dest.ToString().Insert(
                dstart, source.ToString());

            // If the new number is valid, let it be entered into the text field
            if (NumberType == typeof(int))
            {
                if (int.TryParse(text, out int result))
                    return null;
            }
            else if (NumberType == typeof(float))
            {
                if (float.TryParse(text, out float result))
                    return null;
            }
            else if (NumberType == typeof(double))
            {
                if (double.TryParse(text, out double result))
                    return null;
            }

            // Otherwise, replace what they just entered with an empty string
            return new Java.Lang.String(string.Empty);
        }
    }
}