﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MyScout.Android.UI
{
    static class SpreadsheetHelper
    {
        public static Dictionary<string, bool> teamsPicked;
        public static string[] currWhitelist;
        public static int sortingColumn;

        public static int[] FindAllTeamRounds()
        {
            Event currEvent = Event.Current;

            List<int> result = new List<int>();
            for (int i = 0; i < currEvent.Rounds.Count; ++i)
            {
                for (int team = 0; team < currEvent.Rounds[i].TeamData.Length; ++team)
                {
                    if (currEvent.Rounds[i].TeamData[team] != null && currEvent.Rounds[i].TeamData[team].Team.ID == Config.UserTeamID)
                    {
                        result.Add(i);
                        break;
                    }
                }
            }

            return result.ToArray();
        }  

        public static void Clear()
        {
            teamsPicked = null;
            currWhitelist = null;
        }
    }
}