﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using MyScout.Android.Bluetooth;
using System;
using System.IO;
using System.Threading;

namespace MyScout.Android.UI
{
    [Activity(Label = "MyScout", Icon = "@drawable/icon",
        Theme = "@android:style/Theme.Material.NoActionBar")]
    public class ScoutMasterActivity : Activity
    {
        // Variables/Constants
        public static ScoutMasterActivity Instance;
        public static string SyncDatasheetName, SyncDatasheetData;
        public static int WaitingOnScouts = 0;
        public static bool WaitingOnThread = false;

        protected Button[] teamSlots;
        protected Button prevBtn, nextBtn, importTeamsBtn, importRoundsBtn,
			exportBtn, syncBtn, viewCsvBtn, preScoutSyncBtn, startRoundBtn;

        protected TextView roundLbl;
        protected States currentState = States.None;
        protected const int GET_TEAM_REQUEST = 0;
        private static int teamSlotEditIndex;

        const int CSV_VIEW_CODE = 420, CSV_SYNC_CODE = 421,
			CSV_IMPORT_TEAMS_CODE = 422, CSV_IMPORT_ROUNDS_CODE = 423;

        protected enum States
        {
            None, AssigningTeams, ReceivingData
        }

        // Methods
        public static void UpdateUIFromThread()
        {
            if (Instance != null)
            {
                Instance.RunOnUiThread(
                    new Action(Instance.UpdateUI));
            }
        }

        public void SetSyncButtonEnabled(bool enabled)
        {
            syncBtn.Enabled = enabled;
            syncBtn.Text = (enabled) ? "Sync Datasheet" : "Syncing...";
        }

        public void UpdateUI()
        {
            if (WaitingOnScouts > 0)
            {
                startRoundBtn.Text = $"Connecting to Scout {WaitingOnScouts}/{Round.TeamCount}...";
                startRoundBtn.Enabled = false;
            }
            else
            {
                // Update start round button
                if (currentState == States.ReceivingData)
                {
                    // Perform end-of-round actions
                    currentState = States.None;

                    // Go to the next round automatically if there is one
                    if (Event.Current.CurrentRoundIndex < Event.Current.Rounds.Count - 1)
                    {
                        ++Event.Current.CurrentRoundIndex;
                    }

                    // Save the current event
                    // TODO: Do this on another thread
                    Event.Current.Save();

                    Toast.MakeText(this, "Event Saved.",
                        ToastLength.Short).Show();
                }
                else if (currentState == States.AssigningTeams)
                {
                    startRoundBtn.Text = "Finish Round";
                    startRoundBtn.Enabled = true;
                }

                // Enable everything
                prevBtn.Enabled = nextBtn.Enabled = true;

                // Update round navigation bar
                int roundID = Event.Current.CurrentRoundIndex + 1;
                roundLbl.Text = $"Round {roundID}";
                prevBtn.Enabled = (roundID > 1);
                nextBtn.Text = (roundID >= Event.Current.Rounds.Count) ?
                    "+" : ">";

                if (currentState == States.None)
                {
                    startRoundBtn.Text = "Start Round";
                    // TODO: Update text on start round button to say "Redo Round"
                    startRoundBtn.Enabled = ShouldEnableStartRoundBtn();
                }

                // Update team slots
                UpdateTeamSlots();
            }
        }

        public void UpdateTeamSlots()
        {
            // Enable team slots and update text shown on them
            var currentRound = Event.Current.CurrentRound;
            if (currentRound == null) return;

            var teamData = currentRound.TeamData;
            for (int i = 0; i < teamSlots.Length; ++i)
            {
                var teamSlot = teamSlots[i];
                teamSlot.Enabled = true;
                teamSlot.Text = (teamData[i] == null) ?
                    "- No Team Assigned -" : teamData[i].Team.ToString();
            }
        }

        protected bool ShouldEnableStartRoundBtn()
        {
            var currentRound = Event.Current.CurrentRound;
            if (currentRound == null) return false;

            var teamData = currentRound.TeamData;
            if (teamData == null) return false;

            for (int i = 0; i < teamData.Length; ++i)
            {
                if (teamData[i] == null)
                    return false;
            }

            return true;
        }

        // GUI Events
        protected override void OnCreate(Bundle bundle)
        {
            // Go to the event-selection screen if no event has been selected
            if (Event.Current == null)
            {
                OnBackPressed();
                return;
            }

            // Setup GUI
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.ScoutMasterLayout);
            Instance = this;

            // Assign local references to GUI elements
            prevBtn = FindViewById<Button>(Resource.Id.ScoutMasterPrevBtn);
            roundLbl = FindViewById<TextView>(Resource.Id.ScoutMasterRoundLbl);
            nextBtn = FindViewById<Button>(Resource.Id.ScoutMasterNextBtn);

            teamSlots = new Button[Round.TeamCount];
            teamSlots[0] = FindViewById<Button>(Resource.Id.RedTeamSlot1);
            teamSlots[1] = FindViewById<Button>(Resource.Id.RedTeamSlot2);
            teamSlots[2] = FindViewById<Button>(Resource.Id.RedTeamSlot3);
            teamSlots[3] = FindViewById<Button>(Resource.Id.BlueTeamSlot1);
            teamSlots[4] = FindViewById<Button>(Resource.Id.BlueTeamSlot2);
            teamSlots[5] = FindViewById<Button>(Resource.Id.BlueTeamSlot3);

            syncBtn = FindViewById<Button>(Resource.Id.ScoutMasterSyncCsvBtn);
			importTeamsBtn = FindViewById<Button>(Resource.Id.ScoutMasterImportTeamsBtn);
			importRoundsBtn = FindViewById<Button>(Resource.Id.ScoutMasterImportRoundsBtn);
			exportBtn = FindViewById<Button>(Resource.Id.ScoutMasterExportBtn);
            viewCsvBtn = FindViewById<Button>(Resource.Id.ScoutMasterViewCsvBtn);
            preScoutSyncBtn = FindViewById<Button>(Resource.Id.ScoutMasterPreScoutBtn);
            startRoundBtn = FindViewById<Button>(Resource.Id.ScoutMasterStartRoundBtn);

            // Assign events to GUI elements
            prevBtn.Click += PrevBtn_Click;
            nextBtn.Click += NextBtn_Click;

            foreach (var btn in teamSlots)
            {
                btn.Click += TeamSlot_Click;
                btn.LongClick += TeamSlot_LongClick;
            }

            syncBtn.Click += SyncBtn_Click;
			importTeamsBtn.Click += ImportTeamsBtn_Click;
			importRoundsBtn.Click += ImportRoundsBtn_Click;
			exportBtn.Click += ExportBtn_Click;
            viewCsvBtn.Click += ViewCsvBtn_Click;
            preScoutSyncBtn.Click += PreScoutSyncBtn_Click;
            startRoundBtn.Click += StartRoundBtn_Click;

            // Update GUI Elements
            UpdateUI();
        }

        protected override void OnActivityResult(
            int requestCode, [GeneratedEnum]Result resultCode, Intent data)
        {
            if (requestCode == GET_TEAM_REQUEST)
            {
                if (resultCode == Result.Ok)
                {
                    // Set the selected team using the index returned from the team activity
                    int selectedTeamIndex = data.GetIntExtra("SelectedItemIndex", -1);
                    if (selectedTeamIndex < 0) return;

                    var teamData = Event.Current.CurrentRound.TeamData;
                    teamData[teamSlotEditIndex] = new TeamData(selectedTeamIndex);
                }

                UpdateTeamSlots();
                startRoundBtn.Enabled = ShouldEnableStartRoundBtn();
                Event.Current.Save();
            }
            else if (requestCode == CSV_VIEW_CODE && resultCode == Result.Ok)
            {
                int selectedIndex = data.GetIntExtra("SelectedItemIndex", -1);
                var viewIntent = new Intent(this, typeof(SpreadsheetViewActivity));
                string dataSheetDir = Path.Combine(IO.ExternalDataDirectory, "DataSheets");

                Directory.CreateDirectory(dataSheetDir);
                string csvUri = Directory.GetFiles(dataSheetDir)[selectedIndex];

                using (var reader = new StreamReader(csvUri))
                {
                    string csvData = reader.ReadToEnd();
                    viewIntent.PutExtra("csvData", csvData);
                }

                StartActivity(viewIntent);
            }
            else if (requestCode == CSV_SYNC_CODE && resultCode == Result.Ok)
            {
                SetSyncButtonEnabled(false);
                int selectedIndex = data.GetIntExtra("SelectedItemIndex", -1);
                string dataSheetDir = Path.Combine(IO.ExternalDataDirectory, "DataSheets");

                Directory.CreateDirectory(dataSheetDir);
                string csvUri = Directory.GetFiles(dataSheetDir)[selectedIndex];

                using (var reader = new StreamReader(csvUri))
                {
                    SyncDatasheetName = Path.GetFileName(csvUri);
                    SyncDatasheetData = reader.ReadToEnd();
                }

                var connectThread = new BluetoothThread(-1);
                connectThread.SetMode(BluetoothThread.Modes.DataSheet);
                connectThread.Start();
            }
			else if ((requestCode == CSV_IMPORT_TEAMS_CODE ||
				requestCode == CSV_IMPORT_ROUNDS_CODE) && resultCode == Result.Ok)
			{
				int selectedIndex = data.GetIntExtra("SelectedItemIndex", -1);
				string dataSheetDir = Path.Combine(IO.ExternalDataDirectory, "DataSheets");
				string csvUri = Directory.GetFiles(dataSheetDir)[selectedIndex];

				if (requestCode == CSV_IMPORT_TEAMS_CODE)
				{
					Event.Current.ImportTeamsCSV(csvUri);
					UpdateTeamSlots();
				}
				else
				{
					Event.Current.ImportRoundsCSV(csvUri);
					UpdateUI();
				}
			}
		}

        public override void OnBackPressed()
        {
            Event.Current.Save();
            Event.Current = null;
            Finish();
        }

        protected override void OnDestroy()
        {
            Instance = null;
            base.OnDestroy();
        }

        protected void PrevBtn_Click(object sender, EventArgs e)
        {
            if (Event.Current.CurrentRoundIndex > 0)
            {
                --Event.Current.CurrentRoundIndex;
                UpdateUI();
            }
        }

        protected void NextBtn_Click(object sender, EventArgs e)
        {
            // Add a new round if there's no next round to go to
            if (Event.Current.CurrentRoundIndex >= Event.Current.Rounds.Count-1)
            {
                Event.Current.Rounds.Add(new Round());
            }

            ++Event.Current.CurrentRoundIndex;
            UpdateUI();
        }

        protected void TeamSlot_Click(object sender, EventArgs e)
        {
            // Get team slot ID
            var btn = (sender as Button);
            if (btn == null) return;

            int slotID = int.Parse((string)btn.Tag);
            var teamData = Event.Current.CurrentRound.TeamData[slotID];
            teamSlotEditIndex = slotID;
            
            // If no team has been assigned to this slot, open the Team List.
            if (teamData == null)
            {
                StartActivityForResult(
                    typeof(TeamActivity), GET_TEAM_REQUEST);
            }

            // Otherwise, open the Round GUI.
            else
            {
                RoundActivity.CurrentTeam = teamData.Team;
                RoundActivity.TeamSlotIndex = slotID;
                StartActivity(typeof(RoundActivity));
            }
        }

        protected void TeamSlot_LongClick(object sender,
            global::Android.Views.View.LongClickEventArgs e)
        {
            // Get team slot ID
            var btn = (sender as Button);
            if (btn == null) return;

            int slotID = int.Parse((string)btn.Tag);

            // Open the Team List
            teamSlotEditIndex = slotID;
            StartActivityForResult(
                typeof(TeamActivity), GET_TEAM_REQUEST);
        }

        protected void SyncBtn_Click(object sender, EventArgs e)
        {
            StartActivityForResult(typeof(SpreadsheetActivity), CSV_SYNC_CODE);
        }

		protected void ImportTeamsBtn_Click(object sender, EventArgs e)
		{
			StartActivityForResult(typeof(SpreadsheetActivity),
				CSV_IMPORT_TEAMS_CODE);
		}

		protected void ImportRoundsBtn_Click(object sender, EventArgs e)
		{
			StartActivityForResult(typeof(SpreadsheetActivity),
				CSV_IMPORT_ROUNDS_CODE);
		}

		protected void ExportBtn_Click(object sender, EventArgs e)
        {
            // TODO: Make this happen on another thread
            string dataSheetDir = Path.Combine(IO.ExternalDataDirectory, "DataSheets");
            Directory.CreateDirectory(dataSheetDir);

            string filePath = Path.Combine(dataSheetDir,
                $"Event{Event.Current.Index}.csv");
            Event.Current.ExportCSV(filePath);

            Toast.MakeText(this, $"Exported CSV to \"{filePath}\"",
                ToastLength.Long).Show();
        }

        protected void ViewCsvBtn_Click(object sender, EventArgs e)
        {
            StartActivityForResult(typeof(SpreadsheetActivity), CSV_VIEW_CODE);
        }

        protected void PreScoutSyncBtn_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(PreScoutingActivity));
        }

        protected void StartRoundBtn_Click(object sender, EventArgs e)
        {
            // Disable GUI Elements
            prevBtn.Enabled = nextBtn.Enabled = startRoundBtn.Enabled = false;
            foreach (var btn in teamSlots)
            {
                btn.Enabled = false;
            }

            // Send the team data to each scout
            var th = new Thread(new ThreadStart(() =>
            {
                for (int i = 0; i < Round.TeamCount; ++i)
                {
                    // Update UI
                    ++WaitingOnScouts;
                    UpdateUIFromThread();

                    // Connect to Scout
                    BluetoothThread connectThread;
                    if (i >= BluetoothIO.ConnectThreads.Count)
                    {
                        Log.Debug("MyScout", $"Starting Connection {i}");
                        connectThread = new BluetoothThread(i);
                        connectThread.Start();

                        Log.Debug("MyScout", $"Started Connection {i}");
                        BluetoothIO.ConnectThreads.Add(connectThread);
                    }
                    else
                    {
                        Log.Debug("MyScout", $"Connection {i} already exists");
                        connectThread = BluetoothIO.ConnectThreads[i];
                    }

                    // Set Mode
                    if (currentState == States.None)
                    {
                        connectThread.SetMode(
                            BluetoothThread.Modes.Teams);
                    }
                    else if (currentState == States.AssigningTeams)
                    {
                        connectThread.SetMode(
                            BluetoothThread.Modes.RoundData);
                    }

                    WaitingOnThread = true;
                    while (WaitingOnThread) { }

                    Thread.Sleep(2000);
                }

                // Set Mode
                if (currentState == States.None)
                {
                    currentState = States.AssigningTeams;
                }
                else if (currentState == States.AssigningTeams)
                {
                    currentState = States.ReceivingData;
                }

                WaitingOnScouts = 0;
                UpdateUIFromThread();
            }));

            th.Start();
        }
    }
}