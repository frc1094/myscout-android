﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MyScout.Android.UI
{
    internal class ScrollViewLinker : Java.Lang.Object, View.IOnScrollChangeListener, IJavaObject
    {
        ScrollView linkedView;
        
        public ScrollViewLinker(ScrollView linkedView)
        {
            this.linkedView = linkedView;
        }

        public new void Dispose()
        {
            linkedView.Dispose();
        }

        public void OnScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY)
        {
            linkedView.ScrollTo(scrollX, scrollY);
        }
    }
}