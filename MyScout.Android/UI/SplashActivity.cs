﻿using Android.App;
using Android.Graphics.Drawables;
using Android.OS;

namespace MyScout.Android.UI
{
    [Activity(Label = "MyScout", Theme = "@style/SplashTheme",
        MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        // Variables/Constants
        protected Drawable bg;

        // GUI Events
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            StartActivity(typeof(MainActivity));
        }
    }
}