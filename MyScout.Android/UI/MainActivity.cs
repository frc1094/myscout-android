﻿using Android.Animation;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyScout.Android.Bluetooth;
using System;
using System.IO;

namespace MyScout.Android.UI
{
    [Activity(Label = "MyScout", Icon = "@drawable/icon",
        Theme = "@android:style/Theme.Material.NoActionBar")]
    public class MainActivity : Activity
    {
        // Variables/Constants
        public static MainActivity Instance;
        protected RelativeLayout bg;
        protected LinearLayout connectingLayout, startButtonsLayout;
        protected ProgressBar progressCircle;
        protected Button startConnectBtn, preScoutingBtn, settingsBtn, viewSpreadsheetsBtn;
        protected TextView connectingLbl;

        private static string currentStatus = null;
        private static bool setupAdapter = false, startedConnection = false;

        const int CSV_REQ_CODE = 420;

        // GUI Events
        protected override void OnCreate(Bundle bundle)
        {
            Instance = this;

            // Setup GUI
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.ConnectingLayout);

            // Assign local references to GUI elements
            bg = FindViewById<RelativeLayout>(Resource.Id.ConnectingBG);
            connectingLayout = FindViewById<LinearLayout>(Resource.Id.ConnectingLayout);
            connectingLbl = FindViewById<TextView>(Resource.Id.ConnectingLbl);
            progressCircle = FindViewById<ProgressBar>(Resource.Id.ConnectingProgressCircle);

            startButtonsLayout = FindViewById<LinearLayout>(Resource.Id.StartButtonsLayout);
            startConnectBtn = FindViewById<Button>(Resource.Id.StartConnectBtn);
            preScoutingBtn = FindViewById<Button>(Resource.Id.PreScoutingBtn);
            settingsBtn = FindViewById<Button>(Resource.Id.StartSettingsBtn);
            viewSpreadsheetsBtn = FindViewById<Button>(Resource.Id.ViewSpreadsheetsBtn);

            // Assign GUI Events
            startConnectBtn.Click += StartConnectBtn_Click;
            preScoutingBtn.Click += PreScoutingBtn_Click;
            settingsBtn.Click += SettingsBtn_Click;
            viewSpreadsheetsBtn.Click += ViewSpreadsheetsBtn_Click;
            
            // Set the progress bar color
            progressCircle.IndeterminateDrawable.SetColorFilter(
                Color.White, PorterDuff.Mode.Multiply);

            // Set loading text
            if (!string.IsNullOrEmpty(currentStatus))
                connectingLbl.Text = currentStatus;

            Init();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        protected void StartConnectBtn_Click(object sender, EventArgs e)
        {
            startedConnection = true;
            ScoutInit();
        }

        protected void PreScoutingBtn_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(EventActivity));
        }

        protected void SettingsBtn_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(SettingsActivity));
        }

        protected void ViewSpreadsheetsBtn_Click(object sender, EventArgs e)
        {
            var listIntent = new Intent(this, typeof(SpreadsheetActivity));
            StartActivityForResult(listIntent, CSV_REQ_CODE);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch(requestCode)
            {
                case CSV_REQ_CODE:
                    if (resultCode == Result.Ok)
                        ViewCsv(data.GetIntExtra("SelectedItemIndex", -1));
                    break;
            }
        }

        private void ViewCsv(int selectedIndex)
        {
            var viewIntent = new Intent(this, typeof(SpreadsheetViewActivity));
            string dataSheetDir = System.IO.Path.Combine(IO.ExternalDataDirectory, "DataSheets");

            Directory.CreateDirectory(dataSheetDir);
            string csvUri = Directory.GetFiles(dataSheetDir)[selectedIndex];

            using (var reader = new StreamReader(csvUri))
            {
                string csvData = reader.ReadToEnd();
                viewIntent.PutExtra("csvData", csvData);
            }

            StartActivity(viewIntent);
        }

        // Methods
        public static void ShowFishPunFromThread()
        {
            string pun;
            using (var fs = IO.OpenEmbedded(FishPuns.FilePath))
            {
                pun = FishPuns.GetRandomFishPun(fs);
            }

            if (!string.IsNullOrEmpty(pun))
            {
                if (Instance != null && !Instance.IsFinishing && !Instance.IsDestroyed)
                {
                    Instance.SetStatus(pun);
                }
                else
                {
                    currentStatus = pun;
                }
            }
        }

        /// <summary>
        /// Sets text on the UI to display the given status string, can be called outside of main thread
        /// </summary>
        /// <param name="status"></param>
        /// <param name="toast"></param>
        public static void SetStatusFromThread(string status, string toast = null)
        {
            if (Instance != null && !Instance.IsFinishing && !Instance.IsDestroyed)
            {
                Instance.RunOnUiThread(new Action(() =>
                {
                    Instance.SetStatus(status);

                    if (!string.IsNullOrEmpty(toast))
                    {
                        Toast.MakeText(Instance, toast,
                            ToastLength.Long).Show();
                    }
                }));
            }
            else
            {
                currentStatus = status;
            }
        }

        /// <summary>
        /// Displays an error in the main UI thread, can be called outside of main thread
        /// </summary>
        /// <param name="error"></param>
        public static void ShowErrorFromThread(string error)
        {
            if (Instance != null)
            {
                Instance.RunOnUiThread(new Action(() =>
                {
                    Instance.ShowError(error);
                }));
            }
        }

        /// <summary>
        /// Sets text on the UI to display the given status string
        /// </summary>
        /// <param name="status">The string to display</param>
        public void SetStatus(string status)
        {
            if (connectingLbl != null)
                connectingLbl.Text = status;
        }

        /// <summary>
        /// Displays an error to the user
        /// </summary>
        /// <param name="error"></param>
        public void ShowError(string error)
        {
            progressCircle.Visibility = ViewStates.Gone;
            SetStatus($"ERROR: {error}");
        }

        /// <summary>
        /// [Scout Only] Starts a bluetooth thread to listen for Team/DataSet information
        /// </summary>
        public void ScoutStartListening()
        {
            // Start listening for Team/DataSet information
            BluetoothThread connectThread;
            if (BluetoothIO.ConnectThreads.Count < 1 || !BluetoothIO.ConnectThreads[0].Active)
            {
                // Generate a Bluetooth Connection Thread
                connectThread = new BluetoothThread(Config.ScoutMasterAddress);

                // Start a Bluetooth Connection Thread
                connectThread.SetMode(BluetoothThread.Modes.Teams);
                connectThread.Start();
                BluetoothIO.ConnectThreads.Add(connectThread);
            }
            else
            {
                connectThread = BluetoothIO.ConnectThreads[0];
            }

            connectThread.SetMode(
                BluetoothThread.Modes.Teams);
        }

        /// <summary>
        /// Loads or creates the MyScout config file, creates needed directories, and extracts bundled datasets. Afterwards, calls ScoutInit() or ScoutMasterInit() depending on config state.
        /// </summary>
        protected void Init()
        {
            // Load the config file if not already loaded
            if (!Config.Loaded)
            {
                // Attempt to load the config file
                if (!Config.Load())
                {
                    // Loading failed! Go to the first-time setup screen
                    SettingsActivity.IsFirstTimeSetup = true;
                    StartActivity(typeof(SettingsActivity));
                    Finish();
                    return;
                }
            }

            // Create external directories if they don't already exist
            Directory.CreateDirectory(IO.ExternalDataDirectory);
            Directory.CreateDirectory(IO.DataSetDirectory);

            // Copy any Bundled DataSets not already copied
            DataSet.CopyBundledDatasets(this);

            // Call the appropriate method based on the assigned tablet type
            switch (Config.TabletType)
            {
                case Config.TabletTypes.Scout:
                    ScoutInit();
                    break;

                case Config.TabletTypes.ScoutMaster:
                    ScoutMasterInit();
                    break;
            }

            MediaScannerConnection.ScanFile(this, new string[] { IO.ExternalDataDirectory, IO.DataSetDirectory }, null, null);
        }

        /// <summary>
        /// [Scout Only] Prepares for and connects to the ScoutMaster via bluetooth adapter
        /// </summary>
        protected void ScoutInit()
        {
            // Update UI
            if (BluetoothIO.ConnectThreads.Count < 1)
            {
                if (startedConnection)
                {
                    SetStatus("Connecting to Scout Master...");
                }
                else
                {
                    connectingLayout.Visibility = ViewStates.Gone;
                    startButtonsLayout.Visibility = ViewStates.Visible;
                    FadeDrawable(bg.Background, 0, 255, 2000);

                    //var logoFade = FadeDrawable(logo.Drawable, 255, 0, 500, 1000, false);
                    //logoFade.AnimationEnd += LogoFade_AnimationEnd;
                    //logoFade.Start();
                    return;
                }
            }

            connectingLayout.Visibility = ViewStates.Visible;
            startButtonsLayout.Visibility = ViewStates.Gone;
            FadeDrawable(bg.Background, 255, 100, 2000);

            // Prepare the Bluetooth adapter
            if (!setupAdapter)
            {
                if (!BluetoothIO.PrepareAdapter())
                {
                    // Show error message if the device doesn't have a Bluetooth adapter
                    ShowError(BluetoothIO.NoAdapterMessage);
                    return;
                }

                setupAdapter = true;
            }

            // Start listening for team/round data
            ScoutStartListening();
        }

        //private void LogoFade_AnimationEnd(object sender, EventArgs e)
        //{
        //    buttonsLinearLayout.Visibility = ViewStates.Visible;
        //    FadeView(buttonsLinearLayout, 0, 1, 500);
        //}

        protected ObjectAnimator FadeDrawable(Drawable target, int start,
            int end, long duration, long startDelay = 0, bool startNow = true)
        {
            var colorFade = ObjectAnimator.OfPropertyValuesHolder(
                target, PropertyValuesHolder.OfInt("alpha", end));

            target.Alpha = start;
            colorFade.StartDelay = startDelay;
            colorFade.SetTarget(target);
            colorFade.SetDuration(duration);

            if (startNow)
                colorFade.Start();

            return colorFade;
        }

        protected ObjectAnimator FadeView(View target, float start,
            float end, long duration, long startDelay = 0, bool startNow = true)
        {
            var colorFade = ObjectAnimator.OfPropertyValuesHolder(
                target, PropertyValuesHolder.OfFloat("alpha", end));

            target.Alpha = start;
            colorFade.StartDelay = startDelay;
            colorFade.SetTarget(target);
            colorFade.SetDuration(duration);

            if (startNow)
                colorFade.Start();

            return colorFade;
        }

        /// <summary>
        /// [ScoutMaster Only] Starts activity for event management
        /// </summary>
        protected void ScoutMasterInit()
        {
            StartActivity(typeof(EventActivity));
            Finish();
        }
    }
}