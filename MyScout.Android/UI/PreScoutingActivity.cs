﻿using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Widget;
using MyScout.Android.Bluetooth;
using System;
using System.IO;

namespace MyScout.Android.UI
{
    [Activity(Label = "Pre-Scouting", Icon = "@drawable/icon",
        Theme = "@style/MyScoutTheme")]
    public class PreScoutingActivity : ToolbarActivity
    {
        // Variables/Constants
        public static PreScoutingActivity Instance;
        protected Button preScoutBtn, syncBtn, exportBtn;
        protected const int GET_TEAM_REQUEST = 0;

        // Methods
        public void SetSyncButtonEnabled(bool enabled)
        {
            syncBtn.Enabled = enabled;
        }

        public void RefreshUI()
        {
            syncBtn.Text = (Config.TabletType == Config.TabletTypes.ScoutMaster) ?
                "Sync with Scout" : "Sync with Scout Master";
        }

        // GUI Events
        protected override void OnCreate()
        {
            // Setup GUI
            Instance = this;
            SetContentView(Resource.Layout.PreScoutingLayout);

            // Assign local references to GUI elements
            preScoutBtn = FindViewById<Button>(Resource.Id.PreScoutingChooseBtn);
            syncBtn = FindViewById<Button>(Resource.Id.PreScoutingSyncBtn);
            exportBtn = FindViewById<Button>(Resource.Id.ExportPreScoutingCSVBtn);

            // Assign events to GUI elements
            preScoutBtn.Click += AddBtn_Click;
            syncBtn.Click += SyncBtn_Click;
            exportBtn.Click += ExportBtn_Click;

            // Refresh UI
            RefreshUI();
        }

        protected override void OnActivityResult(
            int requestCode, [GeneratedEnum]Result resultCode, Intent data)
        {
            if (requestCode == GET_TEAM_REQUEST)
            {
                if (resultCode == Result.Ok)
                {
                    Event.Current.Save();

                    // Set the selected team using the index returned from the team activity
                    int selectedTeamIndex = data.GetIntExtra("SelectedItemIndex", -1);
                    if (selectedTeamIndex < 0) return;

                    var team = Event.Current.Teams[selectedTeamIndex];
                    RoundActivity.TeamSlotIndex = -1;
                    RoundActivity.CurrentTeam = team;
                    RoundActivity.PreScouting = true;
                    StartActivity(typeof(RoundActivity));
                }
            }
        }

        protected void AddBtn_Click(object sender, EventArgs e)
        {
            StartActivityForResult(
                typeof(TeamActivity), GET_TEAM_REQUEST);
        }

        protected void SyncBtn_Click(object sender, EventArgs e)
        {
            syncBtn.Enabled = false;
            syncBtn.Text = "Syncing...";

            var connectThread = (Config.TabletType == Config.TabletTypes.Scout) ?
                new BluetoothThread(Config.ScoutMasterAddress) : new BluetoothThread(-1);

            connectThread.SetMode(BluetoothThread.Modes.PreScoutingData);
            connectThread.Start();
        }

        protected void ExportBtn_Click(object sender, EventArgs e)
        {
            // TODO: Make this happen on another thread
            string dataSheetDir = Path.Combine(IO.ExternalDataDirectory, "DataSheets");
            Directory.CreateDirectory(dataSheetDir);

            string filePath = Path.Combine(dataSheetDir,
                $"Event{Event.Current.Index}PreScouting.csv");
            Event.Current.ExportPreScoutingCSV(filePath);

            Toast.MakeText(this, $"Exported CSV to \"{filePath}\"",
                ToastLength.Long).Show();
        }
    }
}