﻿using System.Collections.Generic;
using System.IO;
using Android.App;

namespace MyScout.Android.UI
{
    [Activity(Label = "Spreadsheets", Icon = "@drawable/icon",
        Theme = "@style/MyScoutTheme")]
    public class SpreadsheetActivity : ListActivity<string>
    {
        // GUI Events
        protected override void OnCreate()
        {
            base.OnCreate();

            string dataSheetDir = Path.Combine(IO.ExternalDataDirectory, "DataSheets");
            Directory.CreateDirectory(dataSheetDir);

            var fileList = new List<string>(Directory.GetFiles(dataSheetDir));
            for(int i = 0; i < fileList.Count; ++i)
            {
                fileList[i] = fileList[i].Substring(dataSheetDir.Length + 1);
            }

            SetupList(fileList, typeof(string));
        }
    }
}