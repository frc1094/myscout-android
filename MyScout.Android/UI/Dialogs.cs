﻿using System;
using Android.App;
using Android.Content;

namespace MyScout.Android.UI
{
    public static class Dialogs
    {
        // Methods
        public static void MakeYesNoBox(Context context,
            EventHandler<DialogClickEventArgs> yesClickEvent,
            EventHandler<DialogClickEventArgs> noClickEvent)
        {
            var builder = new AlertDialog.Builder(context);
            builder.SetTitle("Remove");
            builder.SetMessage("Are you sure?");
            builder.SetPositiveButton("Yes", yesClickEvent);
            builder.SetNegativeButton("No", noClickEvent);

            var dialog = builder.Create();
            dialog.Show();
        }
    }
}