﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static Android.Support.V7.Widget.Toolbar;

namespace MyScout.Android.UI
{
    [Activity(Label = "Spreadsheet Viewer")]
    public class SpreadsheetViewActivity : Activity
    {
        // Variables/Constants
        public static SpreadsheetViewActivity Instance;
        string csv = "";

        /// <summary>
        /// References to all TextViews
        /// </summary>
        TextView[][] cells;

        /// <summary>
        /// The default arrangement of cells
        /// </summary>
        string[][] baseCells;

        /// <summary>
        /// All possible sorts of the cells, indexed by sorting column-1, row, col
        /// </summary>
        string[][][] sortedCells;
        Thread sortingThread;

        Dictionary<string, bool> teamsPicked;
        Dictionary<string, bool> teamColors;

        private bool RED = true, BLUE = false;

        /// <summary>
        /// The 1-based sorting column.Negative value means descending order, positive ascending.
        /// Actual column is absolute value
        /// </summary>
        int sortingColumn = 0;

        // The record of all columns that have heatmapping enabled
        bool[] heatmappedColumns;
        // The record of all maximum column values
        double[] maxColValues;

        const int ROUND_REQ_CODE = 0;
        const int TEAMID_WIDTH_OVERRIDE = 15;
        const int TEXT_SCALE = 30;
        const int EFFECTIVE_CHAR_WIDTH = TEXT_SCALE / 3;
        const int CELL_TOP_MARGIN = 10;
        const int CELL_RIGHT_MARGIN = 0;

        ColorDrawable BrightOrange { get { return new ColorDrawable(Color.ParseColor("#F36A00")); } }
        ColorDrawable DimOrange { get { return new ColorDrawable(Color.ParseColor("#D84600")); } }
        ColorDrawable DarkOrange { get { return new ColorDrawable(Color.ParseColor("#C33A00")); } }
        ColorDrawable SelectedTeamBGColor { get { return new ColorDrawable(Color.DarkRed); } }
        ColorDrawable RowOddBGColor { get { return new ColorDrawable(Color.DarkGray); } }
        ColorDrawable RowEvenBGColor { get { return new ColorDrawable(Color.DimGray); } }
        ColorDrawable SortedColBGColor { get { return new ColorDrawable(Color.Aqua); } }
        ColorDrawable TeamColorRed { get { return new ColorDrawable(Color.Red); } }
        ColorDrawable TeamColorDarkRed { get { return new ColorDrawable(Color.DarkRed); } }
        ColorDrawable TeamColorBlue { get { return new ColorDrawable(Color.Blue); } }
        ColorDrawable TeamColorDarkBlue { get { return new ColorDrawable(Color.DarkBlue); } }

        LinearLayout content;
        LinearLayout header;
        LinearLayout teamNameList;
        ScrollView teamNameScroll;
        ScrollView contentScroll;

        ScrollViewLinker contentToTeamNameLinker;

        // GUI Events
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Instance = this;
            base.SetContentView(Resource.Layout.SpreadsheetViewLayout);

            //Get a reference to the base csv
            content = FindViewById<LinearLayout>(Resource.Id.baseVertLayout);
            header = FindViewById<LinearLayout>(Resource.Id.csvHeader);
            teamNameList = FindViewById<LinearLayout>(Resource.Id.teamVertLayout);

            contentScroll = FindViewById<ScrollView>(Resource.Id.contentScrollView);
            teamNameScroll = FindViewById<ScrollView>(Resource.Id.teamScrollView);
            
            teamNameScroll.ScrollChange += TeamNameScroll_ScrollChange;

            contentToTeamNameLinker = new ScrollViewLinker(teamNameScroll);

            contentScroll.SetOnScrollChangeListener(contentToTeamNameLinker);

            csv = Intent.GetStringExtra("csvData");
            if(csv.EndsWith("\n"))
            {
                csv = csv.Substring(0, csv.Length - 2);
            }


            Initialize(SpreadsheetHelper.currWhitelist);
        }

        private void TeamNameScroll_LongClick(object sender, View.LongClickEventArgs e)
        {
            ViewSpecificRound();
        }

        private void TeamID_Click(object sender, EventArgs e)
        {
            TextView teamIDView = ((TextView)sender);
            string teamNum = teamIDView.Text;

            if (teamsPicked.ContainsKey(teamNum))
            {
                teamsPicked[teamNum] = !teamsPicked[teamNum];
            }
            else
            {
                teamsPicked.Add(teamNum, true);
            }

            RefreshCells();
        }

        private void HeaderText_Click(object sender, EventArgs e)
        {
            TextView text = (TextView)sender;
            int columnIndex = (int)text.Tag;

            //Switch sortingColumn positive/negative if tapped after already selected
            sortingColumn = (columnIndex + 1) * (Math.Abs(sortingColumn) - 1 == columnIndex && sortingColumn > 0 ? -1 : 1);
            SortRowsBySortingColumn();

            RefreshCells();
        }

        private void HeaderText_LongClick(object sender, View.LongClickEventArgs e)
        {
            int columnIndex = (int)((TextView)sender).Tag;
            heatmappedColumns[columnIndex] = !heatmappedColumns[columnIndex];

            RefreshCells();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.ToolbarSpreadsheetViewMenu, menu);

            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.pickRoundSpreadsheetMenuBtn:
                    ViewSpecificRound();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void TeamNameScroll_ScrollChange(object sender, View.ScrollChangeEventArgs e)
        {
            if (e.OldScrollX == contentScroll.ScrollX && e.OldScrollY == contentScroll.ScrollY)
                ((ScrollView)sender).ScrollTo(e.OldScrollX, e.OldScrollY);
        }

        // Methods
        private void BufferCellData()
        {
            // Buffer all column max values (for heatmapping)
            maxColValues = new double[baseCells[0].Length];
            for (int i = 0; i < baseCells[0].Length; ++i)
            {
                maxColValues[i] = GetMaxColValue(i);
            }

            // Buffer all possible sorted layouts
            sortedCells = new string[baseCells[0].Length][][];
            for(int col = 0; col < baseCells[0].Length; ++col)
            {

                string[][] sortedCellLayout = Copy2DStringArray(baseCells);
                int lastCell = (sortedCellLayout.Length - 1);

                for (int row = lastCell; row >= 1; --row)
                {
                    for (int rowCompare = row; rowCompare < lastCell; ++rowCompare)
                    {
                        double val1, val2;
                        if (col == 0 && teamColors.Count > 0) //If there are team colors, sorts by those instead of by team id
                        {
                            val1 = teamColors[sortedCellLayout[rowCompare][col]] ? 1 : 0;
                            val2 = teamColors[sortedCellLayout[rowCompare + 1][col]] ? 1 : 0;
                        }
                        else
                        {
                            val1 = ToDouble(sortedCellLayout[rowCompare][col]);
                            val2 = ToDouble(sortedCellLayout[rowCompare + 1][col]);
                        }

                        if (val1 < val2)
                        {
                            // Swap positions
                            string[] temp = sortedCellLayout[rowCompare + 1];
                            sortedCellLayout[rowCompare + 1] = sortedCellLayout[rowCompare];
                            sortedCellLayout[rowCompare] = temp;
                        }
                    }
                }

                sortedCells[col] = sortedCellLayout;
            }
        }

        private string[][] Copy2DStringArray(string[][] array)
        {
            string[][] copy = new string[array.Length][];
            for (int i = 0; i < array.Length; ++i)
            {
                copy[i] = new string[array[i].Length];
                for (int j = 0; j < array[i].Length; ++j)
                {
                    copy[i][j] = array[i][j];
                }
            }

            return copy;
        }

        private void Initialize(string[] teamIdWhitelist = null)
        {
            teamColors = new Dictionary<string, bool>();

            teamIdWhitelist = teamIdWhitelist ?? new string[0];
            SpreadsheetHelper.currWhitelist = teamIdWhitelist;

            // Clear the existing views
            header.RemoveAllViews();
            content.RemoveAllViews();
            teamNameList.RemoveAllViews();

            // Re-initialize all the existing team and layout data
            teamColors.Clear();
            SpreadsheetHelper.teamsPicked = SpreadsheetHelper.teamsPicked ?? new Dictionary<string, bool>();
            teamsPicked = SpreadsheetHelper.teamsPicked;

            string[] csvRows = csv.Split('\n');
            List<string[]> baseCellsBuffer = new List<string[]>();

            var columnWidths = new List<int>();
            for (int row = 0; row < csvRows.Length; ++row)
            {
                string[] csvRowValues = csvRows[row].Split(',');

                // If this isn't the header, a whitelist is active and this team row is in the whitelist
                if(row == 0 || (row != 0 && teamIdWhitelist.Contains(csvRowValues[0])) || teamIdWhitelist.Length == 0)
                {
                    // Add to base cell layout
                    baseCellsBuffer.Add(csvRowValues);
                    if(!teamsPicked.ContainsKey(csvRowValues[0]))
                    {
                        teamsPicked.Add(csvRowValues[0], false);
                    }

                    // Find the team colors, if applicable
                    if(row != 0 && teamIdWhitelist.Length > 0)
                    {
                        teamColors.Add(csvRowValues[0], Array.IndexOf(teamIdWhitelist, csvRowValues[0]) < 3 ? RED : BLUE);
                    }
                    
                    // Calculate column widths
                    for (int val = 0; val < csvRowValues.Length; ++val)
                    {
                        if (csvRows[row].Length != 1)
                        {
                            if (columnWidths.Count - 1 >= val)
                            {
                                if (csvRowValues[val].Length > columnWidths[val])
                                {
                                    columnWidths[val] = csvRowValues[val].Length;
                                }
                            }
                            else
                            {
                                columnWidths.Add(csvRowValues[val].Length);
                            }
                        }
                    }
                }
            }

            baseCells = baseCellsBuffer.ToArray();

            sortingThread = new Thread(new ThreadStart(BufferCellData));
            sortingThread.Start();

            heatmappedColumns = new bool[baseCells[0].Length];

            columnWidths[0] = TEAMID_WIDTH_OVERRIDE;

            // For each row in the csv
            cells = new TextView[baseCells.Length][];
            for (int row = 0; row < baseCells.Length; ++row)
            {
                string[] csvRowValues = baseCells[row];
                var rowLayoutParams = new LayoutParams(
                    LayoutParams.WrapContent, LayoutParams.WrapContent)
                {
                    TopMargin = CELL_TOP_MARGIN
                };

                // If this is the header, make it double the height
                rowLayoutParams.Height = (int)(TEXT_SCALE * 1.3);
                if (row == 0)
                    rowLayoutParams.Height *= 2;

                var rowLayout = new LinearLayout(this)
                {
                    Orientation = Orientation.Horizontal,
                    LayoutParameters = rowLayoutParams
                };

                int.TryParse(csvRowValues[0], out int parsedTeamNum);
                cells[row] = new TextView[csvRowValues.Length];

                // For each value in this row, add it to the textview
                for (int col = 0; col < csvRowValues.Length; ++col)
                {
                    var textLayoutParams = new LayoutParams(Convert.ToInt16(
                        columnWidths[col] * EFFECTIVE_CHAR_WIDTH), LayoutParams.MatchParent)
                    {
                        TopMargin = CELL_TOP_MARGIN,
                        Width = Convert.ToInt16(columnWidths[col] * EFFECTIVE_CHAR_WIDTH)
                    };

                    // If this is the header, make it double the height
                    textLayoutParams.Height = (int)(TEXT_SCALE * 1.3);
                    if (row == 0)
                        textLayoutParams.Height *= 2;

                    var text = new TextView(this)
                    {
                        Text = csvRowValues[col],
                        TextSize = TEXT_SCALE,
                        LayoutParameters = textLayoutParams,
                        Tag = col,
                    };

                    text.SetShadowLayer(5, 0, 1, Color.Black);

                    if (row == 0)
                    {
                        text.Click += HeaderText_Click;

                        if (col == 0)
                        {
                            text.LongClick += TeamNameScroll_LongClick;
                        }
                        else
                        {
                            text.LongClick += HeaderText_LongClick;
                        }
                    }

                    cells[row][col] = text;

                    if (col == 0)
                    {
                        if(row != 0)
                            text.Click += TeamID_Click;

                        teamNameList.AddView(text);
                    }
                    else
                    {
                        rowLayout.AddView(text);
                    }
                }

                if (row == 0)
                {
                    rowLayout.Background = BrightOrange;
                    header.AddView(rowLayout);
                }
                else
                {
                    content.AddView(rowLayout);
                    content.SetMinimumWidth(columnWidths.Sum() * EFFECTIVE_CHAR_WIDTH);
                }
            }
            RefreshCells();
        }

        private void RefreshCells()
        {
            for (int row = 0; row < cells.Length; ++row)
            {
                // Set picked team colors
                if (teamsPicked.ContainsKey(cells[row][0].Text) && teamsPicked[cells[row][0].Text])
                {
                    for (int col = 0; col < cells[row].Length; ++col)
                    {
                        cells[row][col].SetTextColor(Color.Gray);
                        cells[row][col].Background = SelectedTeamBGColor;
                    }
                }
                // Set team colors (if they exist)
                else if (teamColors.ContainsKey(cells[row][0].Text))
                {
                    for(int col = 0; col < cells[row].Length; ++col)
                    {
                        if (teamColors[cells[row][0].Text] == RED)
                        {
                            cells[row][col].Background = row % 2 == 0 ? TeamColorRed : TeamColorDarkRed;
                        }
                        else
                        {
                            cells[row][col].Background = row % 2 == 0 ? TeamColorBlue : TeamColorDarkBlue;
                        }
                    }
                }
                // For all non-picked non-colored teams
                else
                {
                    for (int col = 0; col < cells[row].Length; ++col)
                    {
                        cells[row][col].SetTextColor(Color.White);

                        if (row == 0)
                        {
                            cells[row][col].Background = (col == Math.Abs(sortingColumn) - 1) ?
                                SortedColBGColor : BrightOrange;
                        }
                        else
                        {
                            cells[row][col].Background = (row % 2 == 0) ?
                                RowEvenBGColor : RowOddBGColor;

                            if (heatmappedColumns[col])
                            {
                                cells[row][col].Background = Gradient(
                                    ToDouble(cells[row][col].Text), BrightOrange.Color,
                                    maxColValues[col], true);
                            }

                            if (col == Math.Abs(sortingColumn) - 1)
                            {
                                if (row == 0)
                                {
                                    cells[row][col].Background = DimOrange;
                                }

                                if (row != 0)
                                {
                                    cells[row][col].Background = Gradient(
                                        ToDouble(cells[row][col].Text), Color.Aqua,
                                        maxColValues[col], true);
                                }
                                else
                                {
                                    cells[row][col].Background = DarkOrange;
                                }
                            }
                        }

                        if (col == 0)
                        {
                            cells[row][col].Background = (row % 2 == 0) ?
                                DimOrange : BrightOrange;
                        }
                    }
                }
            }
        }

        private double GetMaxColValue(int col)
        {
            double max = 0;
            if (col < baseCells.Length && col >= 0)
            {
                for (int row = 1; row < baseCells.Length; ++row)
                {
                    max = Math.Max(max, ToDouble(baseCells[row][col]));
                }
            }
            return max;
        }

        private void SortRowsBySortingColumn()
        {
            if (!sortingThread.IsAlive)
            {
                string[][] cellContents = Copy2DStringArray(
                    sortedCells[Math.Abs(sortingColumn) - 1]);

                string[][] cellData = cellContents.Skip(1).ToArray();
                if (sortingColumn < 0)
                {
                    cellData = cellData.Reverse().ToArray();
                }

                var final = new List<string[]> { cellContents[0] };
                final.AddRange(cellData);
                ReplaceCellContents(final.ToArray());
            }
        }

        private void ReplaceCellContents(string[][] cellStrings)
        {
            for (int i = 0; i < cells.Length; ++i)
            {
                for (int j = 0; j < cellStrings[i].Length; ++j)
                {
                    cells[i][j].Text = cellStrings[i][j];
                }
            }
        }

        ColorDrawable Gradient(double sample, Color initColor, double max, bool descending)
        {
            var col = new Color(
                CalculateColor(initColor.R),
                CalculateColor(initColor.G),
                CalculateColor(initColor.B));

            return new ColorDrawable(col);

            // Sub-Methods
            int CalculateColor(byte color)
            {
                return (int)Math.Min((color / 5 + (color - (color / 5)) *
                    (sample / ((max != 0) ? max : max + 1))), 255);
            }
        }

        private double ToDouble(string value)
        {
            value = value.Trim(' ', '%');
            value = value.Replace("NaN", "0");
            double.TryParse(value, out double d);
            return d;
        }

        private int GetRowIndexFromTeamNumber(string teamNumber)
        {
            for(int i = 0; i < cells.Length; ++i)
            {
                if(cells[i][0].Text == teamNumber)
                {
                    return i;
                }
            }
            return -1;
        }

        #region Specific Round Data Functions
        protected void ViewSpecificRound()
        {
            var listIntent = new Intent(this, typeof(RoundDatasheetFilterSelectActivity));
            StartActivityForResult(listIntent, ROUND_REQ_CODE);
        }

        protected override void OnActivityResult(int requestCode,
            [GeneratedEnum]Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case ROUND_REQ_CODE:
                    if (resultCode == Result.Ok)
                    {
                        //load the selected round - 1. Option 0 is "all teams".
                        int selectedRound = data.GetIntExtra("SelectedItemIndex", -1);
                        if(selectedRound == 0)
                        {
                            Initialize();
                        }
                        else
                        {
                            int[] rounds = SpreadsheetHelper.FindAllTeamRounds();

                            Initialize(RoundTeamsToStringArray(
                                Event.Current.Rounds[rounds[selectedRound - 1]]));
                        }
                    }
                    break;
            }
        }

        private string[] RoundTeamsToStringArray(Round round)
        {
            var result = new string[round.TeamData.Length];
            for(int i = 0; i < round.TeamData.Length; ++i)
            {
                result[i] = round.TeamData[i].Team.ID;
            }

            return result;
        }
        #endregion
    }
}