﻿using Android.App;
using System.Collections.Generic;
using System.IO;

namespace MyScout.Android.UI
{
    [Activity(Label = "Choose a DataSet", MainLauncher = false,
        Icon = "@drawable/icon", Theme = "@style/MyScoutTheme")]
    public class DataSetActivity : ListActivity<string>
    {
        //  Variables/Constants
        public static List<string> DataSets;

        // GUI Events
        protected override void OnCreate()
        {
            base.OnCreate();

            // Setup List
            DataSets = new List<string>();
            foreach (string dataSet in Directory.GetFiles(
                IO.DataSetDirectory, $"*{DataSet.Extension}"))
            {
                DataSets.Add(Path.GetFileNameWithoutExtension(dataSet));
            }

            SetupList(DataSets, null);
        }
    }
}