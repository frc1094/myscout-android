﻿using System.IO;

namespace MyScout.Android
{
    public static class Config
    {
        // Variables/Constants
        public static string ScoutMasterAddress;
        public static string FilePath
        {
            get
            {
                // Gets the full path to the config file
                return Path.Combine(
                    IO.AppDirectory, FileName);
            }
        }

        //TODO: User team number should be changeable
        public static string UserTeamID = "8069";
        public static TabletTypes TabletType;
        public static bool Loaded = false;

        public const string FileName = "Config.bin";
        private const string Signature = "CFG";
        private const byte Version = 2;

        public enum TabletTypes
        {
            Scout, ScoutMaster
        }

        // Methods
        public static bool Load()
        {
            // Return if the config file doesn't exist
            string filePath = FilePath; // This is so we don't call the FilePath getter twice
            if (!File.Exists(filePath)) return false;

            // Open a stream to the config file and read it's contents
            using (var fileStream = File.OpenRead(filePath))
            using (var reader = new ExtendedBinaryReader(fileStream))
            {
                // Read header
                string sig = reader.ReadSignature(3);
                if (sig != Signature)
                    return false;

                byte version = reader.ReadByte();
                if (version > Version)
                    return false;

                if (version < Version)
                    return false; // TODO: Support old config files?

                // Read config values
                TabletType = (TabletTypes)reader.ReadByte();

                // Read registered devices
                ScoutMasterAddress = reader.ReadString();
            }

            Loaded = true;
            return true;
        }

        public static void Save()
        {
            // Open a stream and write the config file
            using (var fileStream = File.Create(FilePath))
            using (var writer = new ExtendedBinaryWriter(fileStream))
            {
                // Write header
                writer.WriteSignature(Signature);
                writer.Write(Version);

                // Write config values
                writer.Write((byte)TabletType);

                // Write registered devices
                writer.Write(ScoutMasterAddress ?? "");
            }
        }
    }
}