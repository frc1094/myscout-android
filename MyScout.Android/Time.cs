﻿using System;

namespace MyScout.Android
{
    public class Time : Java.Lang.Object
    {
        // Variables/Constants
        public TimeSpan Duration => stopTime - startTime;
        public DateTime StartTime => startTime;
        public DateTime StopTime => stopTime;
        public bool IsTiming => isTiming;

        //protected Timer timer;
        protected DateTime startTime, stopTime;
        protected bool isTiming = false;

        // Constructors
        public Time() { }
        public Time(DateTime start, DateTime stop)
        {
            startTime = start;
            stopTime = stop;
        }

        // Methods
        public void StartTiming()
        {
            if (isTiming)
                return;

            //timer = new Timer();
            //timer.Elapsed += Timer_Elapsed;
            stopTime = startTime = DateTime.Now;
            isTiming = true;
            //timer.Start();
        }

        public void StopTiming()
        {
            //if (timer == null)
            //return;

            if (!isTiming)
                return;
            
            stopTime = DateTime.Now;
            isTiming = false;
            //timer.Stop();
        }

        //protected void Timer_Elapsed(object sender, ElapsedEventArgs e)
        //{
        //    // TODO
        //    e.SignalTime
        //    throw new NotImplementedException();
        //}
    }
}