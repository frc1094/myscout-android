﻿using Android.Content;
using Android.Runtime;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace MyScout.Android
{
    public class DataSet
    {
        // Variables/Constants
        public static DataSet Current;
        public static string CurrentFilePath;

        public List<DataPoint> PreScoutingData = new List<DataPoint>();
        public List<DataPoint> RoundAutoData = new List<DataPoint>();
        public List<DataPoint> RoundTeleOPData = new List<DataPoint>();
        public string Name, Author;
        public float Version = 1;

        public const string BundledDatasetsName = "BundledDatasets", Extension = ".xml";
        public const float MaxSupportedVersion = 1.0f;

        // Methods

        /// <summary>
        /// Extracts datasets packaged with this program into individual .xml files in IO.DataSetDirectory
        /// </summary>
        /// <param name="context"></param>
        public static void CopyBundledDatasets(Context context)
        {
            var bundledDataSets = IO.GetEmbeddedFilesInDir(BundledDatasetsName);

            // Copy data from the Bundled DataSets directory embedded
            // in the APK to the external DataSets directory.
            foreach (var bundledDataSet in bundledDataSets)
            {
                string inPath = $"{BundledDatasetsName}.{bundledDataSet}";
                string outPath = Path.Combine(IO.DataSetDirectory,
                    Path.GetFileName(bundledDataSet));

                // Check version of DataSet if already exists on tablet
                if (File.Exists(outPath))
                {
                    float bundledVer;
                    float ver = GetVersion(outPath);

                    using (var inStream = IO.OpenEmbedded(inPath))
                    {
                        bundledVer = GetVersion(inStream);
                    }

                    // Skip copying if it's unnecessary
                    if (ver >= bundledVer)
                        continue;
                }

                // Copy data from embedded resource to DataSets directory
                using (var inStream = IO.OpenEmbedded(inPath))
                using (var outStream = File.Create(outPath))
                {
                    inStream.CopyTo(outStream);
                }
            }
        }

        public float[] ComputeTotals(List<Round> rounds,
            int teamIndex, out int[] pointCounts)
        {
            var totals = new float[RoundAutoData.Count + RoundTeleOPData.Count];
            var counts = new int[totals.Length];

            foreach (var round in rounds)
            {
                foreach (var teamData in round.TeamData)
                {
                    if (teamData == null || teamData.TeamIndex != teamIndex)
                        continue;

                    // Autonomous
                    int autoPointCount = RoundAutoData.Count;
                    if (teamData.AutoData != null)
                    {
                        for (int i = 0; i < autoPointCount; ++i)
                        {
                            ComputeTotal(teamData.AutoData[i],
                                RoundAutoData[i].DataType, i);
                        }
                    }

                    // Tele-OP
                    if (teamData.TeleOPData != null)
                    {
                        for (int i = 0; i < RoundTeleOPData.Count; ++i)
                        {
                            ComputeTotal(teamData.TeleOPData[i],
                                RoundTeleOPData[i].DataType, i + autoPointCount);
                        }
                    }
                }
            }

            pointCounts = counts;
            return totals;

            // Sub-Methods
            void ComputeTotal(object data, Type type, int i)
            {
                // Booleans
                if (type == typeof(bool))
                {
                    if ((bool)data)
                        ++totals[i];
                }

                // Numbers
                else if (type == typeof(int) || type == typeof(float) ||
                    type == typeof(double))
                {
                    totals[i] += Convert.ToSingle(data);
                }

                // Times
                else if (type == typeof(Time))
                {
                    totals[i] += ComputeTimeTotal((Time)data);
                }
                else if (type == typeof(JavaList<Time>))
                {
                    var times = (JavaList<Time>)data;
                    float total = 0;

                    foreach (var time in times)
                    {
                        total += ComputeTimeTotal(time);
                        ++counts[i]; // TODO: Does this work well?
                    }

                    totals[i] += total;
                    return;
                }
                else return;

                ++counts[i];
            }

            float ComputeTimeTotal(Time time)
            {
                return (time.Duration.Milliseconds / 1000f);
            }
        }

        public static float[] ComputeAverages(float[] totals, int[] counts)
        {
            if (totals.Length != counts.Length)
                throw new Exception("The given total/count arrays have unequal lengths!");

            var averages = new float[totals.Length];
            for (int i = 0; i < averages.Length; ++i)
            {
                averages[i] = (totals[i] / counts[i]);
            }

            return averages;
        }

        public void WriteDataSheetColumns(
            StreamWriter writer, bool isPreScouting = false)
        {
            // Team Column
            writer.Write("Team ID");

            // Data Point Columns
            if (!isPreScouting)
            {
                foreach (var point in RoundAutoData)
                {
                    point.WriteCSVColumn(writer);
                }

                foreach (var point in RoundTeleOPData)
                {
                    point.WriteCSVColumn(writer);
                }
            }
            else
            {
                foreach (var point in PreScoutingData)
                {
                    writer.Write($",{point.Name}");
                }
            }

            writer.WriteLine();
        }

        public void WriteDataSheetRow(StreamWriter writer,
            float[] totals, float[] averages)
        {
            if (totals.Length != averages.Length)
                throw new Exception("The given total/average arrays have unequal lengths!");

            // Autonomous
            int autoPointCount = RoundAutoData.Count;
            for (int i = 0; i < autoPointCount; ++i)
            {
                WriteDataPoint(RoundAutoData[i].DataType, i);
            }

            // Tele-OP
            for (int i = 0; i < RoundTeleOPData.Count; ++i)
            {
                WriteDataPoint(RoundTeleOPData[i].DataType, i + autoPointCount);
            }

            // Sub-Methods
            void WriteDataPoint(Type type, int i)
            {
                // Booleans
                if (type == typeof(bool))
                {
                    writer.Write($",{(int)(averages[i] * 100)}%");
                }

                // Numbers
                else if (type == typeof(bool) || type == typeof(int) ||
                    type == typeof(float) || type == typeof(double) ||
                    type == typeof(Time) || type == typeof(JavaList<Time>))
                {
                    writer.Write($",{totals[i]}");
                    writer.Write($",{averages[i]}");
                }

                // Other
                else
                {
                    writer.Write(",");
                }
            }
        }

        public static Type GetDataType(string type)
        {
            // Returns a System.Type from a string
            // Supports booleans, strings, Times, and some primitive number types
            // Nothing else is needed for DataPoints
            switch (type.ToLower())
            {
                case "bool":
                case "boolean":
                    return typeof(bool);

                case "int":
                case "integer":
                case "int32":
                case "sint32":
                    return typeof(int);

                case "float":
                case "single":
                    return typeof(float);

                case "double":
                    return typeof(double);

                case "string":
                    return typeof(string);

                case "time":
                    return typeof(Time);

                case "times":
                    return typeof(JavaList<Time>);
            }

            return null;
        }

        public static float GetVersion(string filePath)
        {
            using (var fs = File.OpenRead(filePath))
            {
                return GetVersion(fs);
            }
        }

        public static float GetVersion(Stream fileStream)
        {
            float ver = 0;
            var xml = XDocument.Load(fileStream); // TODO: Maybe use XReader for this?
            var versionAttr = xml.Root.Attribute("version");

            if (versionAttr != null)
                float.TryParse(versionAttr.Value, out ver);

            return ver;
        }

        public void Load(string filePath)
        {
            CurrentFilePath = filePath;
            using (var fs = File.OpenRead(filePath))
            {
                Load(fs, Path.GetFileNameWithoutExtension(filePath));
            }
        }

        public void Load(Stream fileStream, string defaultName = null)
        {
            // TODO: Optimize this method
            // Get root attributes
            var xml = XDocument.Load(fileStream);
            var root = xml.Root;

            var nameAttr = root.Attribute("name");
            var versionAttr = root.Attribute("version");
            var fmtAttr = root.Attribute("format");
            var authorAttr = root.Attribute("author");

            if (versionAttr != null)
                float.TryParse(versionAttr.Value, out Version);

            // Check DataSet version
            if (fmtAttr != null && float.TryParse(fmtAttr.Value, out float fmt))
            {
                // Return if the loaded DataSet's version is unsupported
                if (fmt > MaxSupportedVersion) return;
            }

            // Assign name and author variables
            Name = (nameAttr == null) ? defaultName : nameAttr.Value;
            Author = authorAttr?.Value;

            // Load Pre-Scouting Data
            var preScoutData = root.Element("PreScoutData");
            ReadDataPoints(preScoutData, PreScoutingData);

            // Get Round Data elements
            var roundData = root.Element("RoundData");
            if (roundData == null) return;

            // Load Autonomous Round Data
            var autoRoundData = roundData.Element("Autonomous");
            ReadDataPoints(autoRoundData, RoundAutoData);

            // Load Tele-OP Round Data
            var teleOPRoundData = roundData.Element("TeleOp");
            ReadDataPoints(teleOPRoundData, RoundTeleOPData);
        }

        protected void ReadDataPoints(XElement element, List<DataPoint> pointList)
        {
            if (element == null) return;
            foreach (var point in element.Elements("Data"))
            {
                // Get attributes from XML
                var nameAttr = point.Attribute("name");
                var typeAttr = point.Attribute("type");
                if (nameAttr == null || typeAttr == null) continue;

                // Get data type from string in XML
                var type = GetDataType(typeAttr.Value);
                if (type == null || string.IsNullOrEmpty(nameAttr.Value)) continue;

                int weight = -1;
                var weightAttr = point.Attribute("weight");
                if (weightAttr != null && !string.IsNullOrEmpty(weightAttr.Value))
                    int.TryParse(weightAttr.Value, out weight);

                // Get Multiple Choices (if any)
                var choices = new Dictionary<string, DataPoint.Choice>();
                foreach (var choice in point.Elements("Choice"))
                {
                    var choiceNameAttr = choice.Attribute("name");
                    var choiceValueAttr = choice.Attribute("value");

                    var v = Convert.ChangeType(choiceValueAttr.Value, type);
                    choices.Add(choiceNameAttr.Value, new DataPoint.Choice(v));
                }

                // Make data point from loaded information and add it to the given list
                pointList.Add(new DataPoint(nameAttr.Value,
                    type, weight, choices));
            }
        }

        public void FillPreScoutGUI(LinearLayout layout)
        {
            FillLinearLayout(layout, PreScoutingData);
        }

        public void FillPreScoutingGUI(LinearLayout layout)
        {
            FillLinearLayout(layout, PreScoutingData);
        }

        public void FillAutonomousGUI(LinearLayout layout)
        {
            FillLinearLayout(layout, RoundAutoData);
        }

        public void FillTeleOPGUI(LinearLayout layout)
        {
            FillLinearLayout(layout, RoundTeleOPData);
        }

        protected void FillLinearLayout(
            LinearLayout layout, List<DataPoint> points)
        {
            var context = layout.Context;
            foreach (var point in points)
            {
                point.AddGUIWidget(context, layout);
            }
        }
    }
}