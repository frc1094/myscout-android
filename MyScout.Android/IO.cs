﻿using Android.OS;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Android.App;

namespace MyScout.Android
{
    public static class IO
    {
        // Variables/Constants
        public static string AppDirectory
        {
            get => System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
        }

        public static string ExternalDataDirectory
        {
            get => Path.Combine(
                    //Environment.ExternalStorageDirectory.AbsolutePath, "MyScout");
                    Application.Context.GetExternalFilesDir(null).ToString(), "MyScout");
        }

        public static string DataSetDirectory
        {
            get => Path.Combine(ExternalDataDirectory, "DataSets");
        }

        public const string EmbeddedPrefix = "MyScout.Android.";

        // Methods
        public static byte[] ReadInChunks(Stream stream,
            int length, int chunkSize)
        {
            // Read the data through several smaller pieces
            byte[] data = new byte[length];
            int bytes = 0;

            while (bytes < length)
            {
                int size = ((length - bytes) >= chunkSize) ?
                    chunkSize : length - bytes;

                int received = stream.Read(data, bytes, size);
                bytes += received;
            }

            return data;
        }

        public static void WriteInChunks(Stream stream,
            byte[] data, int chunkSize)
        {
            // Write the given data in several smaller pieces
            int bytes = 0, dataLength = data.Length;
            while (bytes < dataLength)
            {
                int size = ((bytes + chunkSize) <= dataLength) ?
                    chunkSize : dataLength - bytes;

                stream.Write(data, bytes, size);
                bytes += size;
            }
        }

        public static Stream OpenEmbedded(string filePath)
        {
            // Get a resource from within the MyScout.Android Assembly
            var asm = Assembly.GetExecutingAssembly();
            return asm.GetManifestResourceStream($"{EmbeddedPrefix}{filePath}");
        }

        public static string[] GetEmbeddedFiles()
        {
            var asm = Assembly.GetExecutingAssembly();
            return asm.GetManifestResourceNames();
        }

        public static List<string> GetEmbeddedFilesInDir(string dir)
        {
            var files = new List<string>();
            var embedded = GetEmbeddedFiles();
            string prefix = $"{EmbeddedPrefix}{dir}.";
            int prefixLen = prefix.Length;

            foreach (var e in embedded)
            {
                if (e.StartsWith(prefix))
                    files.Add(e.Substring(prefixLen));
            }

            return files;
        }
    }
}