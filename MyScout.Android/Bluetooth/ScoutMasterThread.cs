﻿//using Android.Util;
//using Java.Lang;
//using MyScout.Android.UI;
//using System.IO;

//namespace MyScout.Android.Bluetooth
//{
//    public class ScoutMasterThread : BluetoothThread
//    {
//        // Methods
//        protected override void UpdateLoop()
//        {
//            for (int i = 0; i < Config.RegisteredDevices.Count; ++i)
//            {
//                // Establish connection with scout
//                var address = Config.RegisteredDevices[i];
//                var device = adapter.GetRemoteDevice(address);

//                var serverSocket = adapter.ListenUsingRfcommWithServiceRecord(
//                    "MSServer", BluetoothIO.GetUUID(address));

//                try
//                {
//                    Print($"Waiting for connection from {address}...");
//                    socket = serverSocket.Accept();
//                    reader = new ExtendedBinaryReader(socket.InputStream);
//                    writer = new ExtendedBinaryWriter(socket.OutputStream);
//                    Print($"Connection established with {address}");

//                    try
//                    {
//                        serverSocket.Close();
//                        Print("Closed server socket");
//                    }
//                    catch { }
//                }
//                catch (Exception ex)
//                {
//                    #if DEBUG
//                        Log.Error("MyScout",
//                            $"ERROR: Couldn't establish connection. {ex.Message}");
//                    #endif
//                }

//                // Perform the current action on this scout
//                Print("About to perform action");
//                if (socket != null)
//                    DoCurrentAction(i);

//                Print("done performing action");

//                // Close Socket
//                Print("closing socket");
//                CloseSocket(true);
//                Sleep(500); // TODO: See how much this can be lowered
//            }

//            currentAction = Actions.None;
//        }

//        protected override void AssignTeams(int deviceIndex)
//        {
//            Print("Assigning Teams...");
//            // Send team data to connected scout
//            var round = Event.Current.CurrentRound;
//            var team = round.TeamData[deviceIndex].Team;
//            writer.Write(team);

//            // Send DataSet FileName
//            writer.Write(Event.Current.DataSetFileName);

//            // Wait to see if we need to send the DataSet over, or if the scout already has it
//            bool hasDataSet = reader.ReadBoolean();
//            if (!hasDataSet)
//            {
//                // Send the DataSet in it's entirity
//                // TODO: Maybe compress this?
//                string dataSetPath = Path.Combine(
//                    IO.DataSetDirectory, Event.Current.DataSetFileName);

//                var data = File.ReadAllBytes(dataSetPath);
//                writer.Write(data.Length);
//                writer.WriteInChunks(data, BluetoothIO.ChunkSize);
//            }

//            Print("Done");
//        }

//        protected override void SendRoundData(int deviceIndex)
//        {
//            var dataSet = DataSet.Current;
//            var round = Event.Current.CurrentRound;
//            var teamData = round.TeamData[deviceIndex];

//            // Wait until ready
//            if (!reader.ReadBoolean()) return;

//            // Get Autonomous Round Data
//            teamData.AutoData = new object[dataSet.RoundAutoData.Count];
//            for (int i = 0; i < dataSet.RoundAutoData.Count; ++i)
//            {
//                var point = dataSet.RoundAutoData[i];
//                teamData.AutoData[i] = reader.ReadByType(point.DataType);
//            }

//            // Get Tele-OP Round Data
//            teamData.TeleOPData = new object[dataSet.RoundTeleOPData.Count];
//            for (int i = 0; i < dataSet.RoundTeleOPData.Count; ++i)
//            {
//                var point = dataSet.RoundTeleOPData[i];
//                teamData.TeleOPData[i] = reader.ReadByType(point.DataType);
//            }
//        }

//        protected override void UpdateUI()
//        {
//            --ScoutMasterActivity.WaitingOnScouts;
//            ScoutMasterActivity.UpdateUIFromThread();
//        }
//    }
//}