﻿using Android.Bluetooth;
using Java.Util;
using MyScout.Android.Bluetooth;
using System.Collections.Generic;

// TODO: Change the namespace to MyScout.Android.Bluetooth
namespace MyScout.Android
{
    public static class BluetoothIO
    {
        // Variables/Constants
        public static List<BluetoothThread> ConnectThreads = new List<BluetoothThread>();
        //public static BluetoothThread ConnectThread;
        public static BluetoothAdapter Adapter
        {
            get
            {
                // Get the default bluetooth adapter
                if (adapter == null)
                    GetBluetoothAdapter();

                return adapter;
            }
        }

        private static BluetoothAdapter adapter = null;
        public const string MyScoutUUID = "5a38ec12-e134-4318-84dd-b2a43246ebc0",
            NoAdapterMessage = "ERROR: This device does not have a Bluetooth adapter!";
        public const int ChunkSize = 2048;
        //private const string UUIDPrefix = "51dc3703-9458-4216-b2a3-";

        // Methods
        //public static UUID GetUUID(string address)
        //{
        //    // Returns a string that was randomly-generated, but with the MAC
        //    // Address at the end. This is used for identifying BT connections.
        //    string add = address.Replace(":", "");
        //    string str = $"{UUIDPrefix}{add}";

        //    return UUID.FromString(str);
        //}

        public static UUID GetUUID()
        {
            return UUID.FromString(MyScoutUUID);
        }

        public static bool PrepareAdapter()
        {
            // Return if the device doesn't have a Bluetooth adapter
            if (Adapter == null)
                return false;

            // Enable the Bluetooth adapter if not already enabled
            if (!adapter.IsEnabled)
                adapter.Enable();

            // Cancel device discovery as it slows down a Bluetooth connection
            adapter.CancelDiscovery();
            return true;
        }

        private static void GetBluetoothAdapter()
        {
            // This was implemented as a method in-case support for multiple
            // Bluetooth adapters gets added to Android in the future.
            adapter = BluetoothAdapter.DefaultAdapter;
        }
    }
}