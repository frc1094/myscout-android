﻿using Android.Bluetooth;
using Android.Util;
using System;

namespace MyScout.Android.Bluetooth
{
    public class Device : IDisposable
    {
        // Variables/Constants
        // We define these as properties so as to keep them read-only publicly
        public BluetoothSocket Socket => socket;
        public ExtendedBinaryReader Reader => reader;
        public ExtendedBinaryWriter Writer => writer;

        protected BluetoothAdapter adapter;
        protected BluetoothSocket socket;
        protected ExtendedBinaryReader reader;
        protected ExtendedBinaryWriter writer;

        // Constructors
        public Device() { }
        public Device(BluetoothSocket socket)
        {
            this.socket = socket;
            reader = new ExtendedBinaryReader(socket.InputStream);
            writer = new ExtendedBinaryWriter(socket.OutputStream);
        }

        // Methods
        public bool Connect(string address)
        {
            try
            {
                adapter = BluetoothIO.Adapter;

                // Attempt to open a socket
                var device = adapter.GetRemoteDevice(address);
                socket = device.CreateRfcommSocketToServiceRecord(
                    BluetoothIO.GetUUID());

                Log.Debug("MyScout", $"Connecting to scout master {address}, UUID: {BluetoothIO.GetUUID()}");
                socket.Connect();

                // Create ExtendedBinaryReader and ExtendedBinaryWriters
                if (socket != null && socket.IsConnected)
                {
                    reader = new ExtendedBinaryReader(socket.InputStream);
                    writer = new ExtendedBinaryWriter(socket.OutputStream);
                }
            }
            catch (Exception ex)
            {
                #if DEBUG
                    Log.Error("MyScout",
                        $"ERROR: Could not connect. {ex.Message}");
                #endif

                // Close the socket/reader/writer if the connection failed
                Close();
            }

            // Return whether or not the connection was successful
            return (socket != null && socket.IsConnected);
        }

        public void Close()
        {
            try
            {
                // Close reader/writer and socket
                //if (reader != null)
                //    reader.Close();

                //if (writer != null)
                //    writer.Close();

                if (socket != null)
                {
                    //if (socket.InputStream != null)
                    //    socket.InputStream.Close();

                    //if (socket.OutputStream != null)
                    //    socket.OutputStream.Close();

                    socket.Close();
                }

                reader = null;
                writer = null;
                socket = null;
            }
            catch { }
        }

        public void Dispose()
        {
            Close();
        }
    }
}