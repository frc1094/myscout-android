﻿//using Android.Bluetooth;
//using MyScout.Android.UI;
//using System;
//using System.IO;

//namespace MyScout.Android.Bluetooth
//{
//    public class ScoutThread : BluetoothThread
//    {
//        // Variables/Constants
//        // TODO

//        // Methods
//        protected override BluetoothSocket Connect()
//        {
//            mode = Modes.Teams;

//            // Ensure a Scout Master has been registered
//            if (Config.RegisteredDevices.Count < 1)
//            {
//                active = false;
//                throw new Exception("No Scout Master has been registered.");
//            }

//            // Get the address of the registered Scout Master
//            var address = Config.RegisteredDevices[0];
//            var device = BluetoothIO.Adapter.GetRemoteDevice(address);

//            // Connect to the Scout Master
//            var s = device.CreateRfcommSocketToServiceRecord(
//                BluetoothIO.GetUUID(adapter.Address));

//            s.Connect();
//            return s;
//        }

//        protected override void TeamsMode()
//        {
//            GetTeams();
//            AssignTeams();
//            // TODO
//        }

//        protected void GetTeams()
//        {
//            // Update UI
//            // TODO: Maybe change this text
//            MainActivity.SetStatusFromThread(
//                "Waiting for Scout Master to assign team...");

//            // Read team data from Scout Master
//            var team = reader.ReadTeam();
//            RoundActivity.CurrentTeam = team;

//            // Read the file name of the DataSet
//            var dataSetFileName = reader.ReadString();
//            string dataSetFilePath = Path.Combine(
//                IO.DataSetDirectory, dataSetFileName);

//            // Don't bother if we already have the DataSet loaded
//            if (DataSet.CurrentFilePath != dataSetFilePath)
//            {
//                // Let the Scout Master know if we need the DataSet
//                bool hasDataSet = File.Exists(dataSetFilePath);
//                writer.Write(hasDataSet);

//                if (!hasDataSet)
//                {
//                    // Download the DataSet from the Scout Master
//                    int fileSize = reader.ReadInt32();
//                    var data = reader.ReadInChunks(fileSize, BluetoothIO.ChunkSize);

//                    // Save the downloaded DataSet to the DataSets directory and load it
//                    File.WriteAllBytes(dataSetFilePath, data);
//                }

//                // Load the DataSet
//                var dataSet = new DataSet();
//                dataSet.Load(dataSetFilePath);
//                DataSet.Current = dataSet;
//            }
//            else
//            {
//                // Let the Scout Master know we already have the DataSet
//                writer.Write(true);
//            }

//            // Go to Round UI
//            if (MainActivity.Instance != null)
//            {
//                MainActivity.Instance.RunOnUiThread(() =>
//                {
//                    MainActivity.Instance.StartActivity(typeof(RoundActivity));
//                    MainActivity.Instance.Finish();
//                });
//            }
//        }

//        protected void AssignTeams()
//        {
//            Print("Assigning Teams...");
//            // Send team data to connected scout
//            var round = Event.Current.CurrentRound;
//            var team = round.TeamData[deviceIndex].Team;
//            writer.Write(team);

//            // Send DataSet FileName
//            writer.Write(Event.Current.DataSetFileName);

//            // Wait to see if we need to send the DataSet over, or if the scout already has it
//            bool hasDataSet = reader.ReadBoolean();
//            if (!hasDataSet)
//            {
//                // Send the DataSet in it's entirity
//                // TODO: Maybe compress this?
//                string dataSetPath = Path.Combine(
//                    IO.DataSetDirectory, Event.Current.DataSetFileName);

//                var data = File.ReadAllBytes(dataSetPath);
//                writer.Write(data.Length);
//                writer.WriteInChunks(data, BluetoothIO.ChunkSize);
//            }

//            Print("Done");
//        }

//        protected override void SendRoundData(int deviceIndex)
//        {
//            // Get UI Data
//            if (RoundActivity.Instance == null)
//            {
//                writer.Write(false);
//                throw new Exception("Round Activity Instance was null");
//            }

//            var dataSet = DataSet.Current;
//            object[] roundAutoData = null, roundTeleOPData = null;
//            bool ready = false;

//            RoundActivity.Instance.RunOnUiThread(new Action(() =>
//            {
//                roundAutoData = RoundActivity.Instance.GetAutoData();
//                roundTeleOPData = RoundActivity.Instance.GetTeleOPData();

//                RoundActivity.Instance.StartActivity(typeof(MainActivity));
//                RoundActivity.Instance.Finish();
//                ready = true;
//            }));

//            // Block thread until UI thread gives us the data we need
//            while (!ready) { }

//            // Let scout master know we're ready
//            writer.Write(true);

//            // Update UI
//            MainActivity.SetStatusFromThread(
//                "Sending round data to Scout Master...");

//            // Write Autonomous Data
//            for (int i = 0; i < roundAutoData.Length; ++i)
//            {
//                writer.WriteByType(roundAutoData[i],
//                    dataSet.RoundAutoData[i].DataType);
//            }

//            // Write Tele-OP Data
//            for (int i = 0; i < roundTeleOPData.Length; ++i)
//            {
//                writer.WriteByType(roundTeleOPData[i],
//                    dataSet.RoundTeleOPData[i].DataType);
//            }

//            //active = false;
//            currentAction = Actions.None;

//            if (MainActivity.Instance != null)
//            {
//                MainActivity.SetStatusFromThread("Waiting...",
//                    "Sent data to Scout Master!");
//                MainActivity.Instance.ScoutStartListening();
//            }

//            Sleep(5000);

//            if (MainActivity.Instance != null)
//            {
//                MainActivity.SetStatusFromThread(
//                    "Connecting to Scout Master...");
//            }
//        }
//    }
//}