﻿using Android.Bluetooth;
using Android.Content;
using Android.Util;
using Android.Widget;
using Java.Lang;
using MyScout.Android.UI;
using System.Collections.Generic;
using System.IO;

namespace MyScout.Android.Bluetooth
{
    public class BluetoothThread : Thread
    {
        // Variables/Constants
        public Device Device;
        public enum Modes
        {
            None, Teams, RoundData, PreScoutingData, DataSheet
        }

        public bool Active => active;
        protected string dataSetFileName, deviceAddress;
        protected int teamIndex;
        protected Modes mode = Modes.None;
        protected bool active = false, threadClosed = true, isScoutMaster = false;
        private static BluetoothServerSocket serverSocket;

        // Constructors
        public BluetoothThread(int teamIndex)
        {
            isScoutMaster = true;
            this.teamIndex = teamIndex;
        }

        public BluetoothThread(string address)
        {
            isScoutMaster = false;
            deviceAddress = address;
        }

        // Methods
        public void SetMode(Modes m)
        {
            mode = m;
        }

        public override void Start()
        {
            // Close any existing sockets/readers/writers
            if (active)
                Close();

            // Initialize variables
            active = true;
            threadClosed = false;
            base.Start();
        }

        public void Close()
        {
            // Block until the thread closes
            active = false;
            while (!threadClosed) { }

            // Close any open sockets/readers/writers
            if (Device != null)
                Device.Close();
        }

        public override sealed void Run()
        {
            // Establish connections
            if (!Connect())
                active = false;

            var reader = Device.Reader;
            var writer = Device.Writer;
            Print("Connected");

            ScoutMasterActivity.WaitingOnThread = false;

            // Update Loop
            while (active)
            {
                try
                {
                    if (Device == null)
                        break;

                    // Block until we get the current mode from the parent
                    if (isScoutMaster)
                    {
                        if (mode == Modes.None) continue;
                        Print($"Mode == {mode.ToString()}");

                        // Send current mode to scout
                        if (mode != Modes.PreScoutingData)
                        {
                            Print("writing byte");
                            writer.Write((byte)mode);
                            Print("wrote byte");
                        }

                        // Perform mode-specific tasks
                        switch (mode)
                        {
                            case Modes.Teams:
                                var round = Event.Current.CurrentRound;
                                AssignTeam(round.TeamData[teamIndex].Team, reader, writer);
                                ScoutMasterActivity.WaitingOnThread = false;
                                break;

                            case Modes.RoundData:
                                Print("getting round data");
                                GetRoundData(reader);
                                Print("got round data");
                                ScoutMasterActivity.WaitingOnThread = false;
                                break;

                            case Modes.PreScoutingData:
                                Print("getting pre-scouting data");
                                GetPreScoutingData(reader);
                                Print("got pre-scouting data");

                                // Let Scout know we got it all
                                writer.Write(true);
                                Sleep(1000);

                                // Close any open sockets/readers/writers
                                active = false;
                                if (Device != null)
                                    Device.Close();

                                break;

                            case Modes.DataSheet:
                                Print("sending datasheet");
                                SendDatasheet(writer);
                                Print("sent datasheet");

                                // Block until scout gets everything
                                bool gotAll = reader.ReadBoolean();
                                if (!gotAll)
                                    continue;

                                // Close any open sockets/readers/writers
                                active = false;
                                if (Device != null)
                                    Device.Close();

                                break;
                        }
                    }
                    else
                    {
                        if (mode != Modes.PreScoutingData)
                        {
                            Print("reading byte");
                            mode = (Modes)reader.ReadByte();
                            Print("read byte");
                        }

                        // Perform mode-specific tasks
                        switch (mode)
                        {
                            case Modes.Teams:
                                Print("getting teams");
                                GetTeam(reader, writer);
                                break;

                            case Modes.RoundData:
                                Print("sending round data");
                                SendRoundData(writer);
                                Print("sent round data");
                                break;

                            case Modes.PreScoutingData:
                                Print("sending pre-scouting data");
                                SendPreScoutingData(writer);
                                Print("sent pre-scouting data");

                                // Block until scout master gets everything
                                bool gotAll = reader.ReadBoolean();
                                if (!gotAll)
                                    continue;

                                // Close any open sockets/readers/writers
                                active = false;
                                if (Device != null)
                                    Device.Close();

                                break;

                            case Modes.DataSheet:
                                Print("getting datasheet");
                                GetDatasheet(reader);
                                Print("got datasheet");

                                // Let Scout Master know we got it all
                                writer.Write(true);
                                Sleep(1000);

                                // Close any open sockets/readers/writers
                                active = false;
                                if (Device != null)
                                    Device.Close();

                                break;
                        }
                    }

                    mode = Modes.None;
                }
                catch (Exception ex)
                {
                    #if DEBUG
                        Log.Error("MyScout",
                            $"ERROR: Exception thrown in UpdateLoop. {ex.Message}");
                    #endif
                }
            }

            Print("Closing thread");
            threadClosed = true;
        }

        protected bool Connect()
        {
            Print($"Connect method");
            BluetoothIO.Adapter.CancelDiscovery();

            while (active)
            {
                Print("Connect attempt");

                // Attempt to connect, then break out of the loop if the
                // connection attempt was successful.
                if (isScoutMaster)
                {
                    if (serverSocket == null)
                    {
                        var adapter = BluetoothAdapter.DefaultAdapter;
                        serverSocket = adapter.ListenUsingRfcommWithServiceRecord(
                            "MyScout", BluetoothIO.GetUUID());
                    }

                    try
                    {
                        var socket = serverSocket.Accept();
                        if (socket != null/* && socket.IsConnected*/)
                        {
                            Device = new Device(socket);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Print($"ERROR: {ex.Message}");

                        //if (serverSocket != null)
                        //    serverSocket.Close();

                        //serverSocket = null;
                    }
                }
                else
                {
                    if (Device == null)
                        Device = new Device();

                    if (Device.Connect(deviceAddress))
                        return true;
                }
                
                // If the attempt failed, block for some time, then try again.
                Sleep(1000); // TODO: Maybe make this random?
            }

            return false;
        }

        protected void GetTeam(ExtendedBinaryReader reader,
            ExtendedBinaryWriter writer)
        {
            // Update UI
            MainActivity.SetStatusFromThread(
                "Reading Team from Scout Master...");

            // Read the file name of the DataSet
            dataSetFileName = reader.ReadString();
            string dataSetFilePath = Path.Combine(
                IO.DataSetDirectory, dataSetFileName);

            float dataSetVersion = reader.ReadSingle();

            // Don't bother if we already have the DataSet loaded
            if (DataSet.CurrentFilePath != dataSetFilePath)
            {
                // Let Parent know if we need the DataSet
                bool hasDataSet = File.Exists(dataSetFilePath);
                if (hasDataSet)
                {
                    float ver = DataSet.GetVersion(dataSetFilePath);
                    hasDataSet = (ver == dataSetVersion);
                }

                writer.Write(hasDataSet);

                if (!hasDataSet)
                {
                    // Download the DataSet from Parent
                    int fileSize = reader.ReadInt32();
                    var data = reader.ReadInChunks(fileSize, BluetoothIO.ChunkSize);

                    // Save the downloaded DataSet to the DataSets directory and load it
                    File.WriteAllBytes(dataSetFilePath, data);
                }

                // Load the DataSet
                var dataSet = new DataSet();
                dataSet.Load(dataSetFilePath);
                DataSet.Current = dataSet;
            }
            else
            {
                // Let Parent know we already have the DataSet
                writer.Write(true);
            }

            // Get Team Data
            RoundActivity.CurrentTeam = reader.ReadTeam();

            // Go to Round UI
            if (MainActivity.Instance != null)
            {
                MainActivity.Instance.RunOnUiThread(() =>
                {
                    MainActivity.Instance.StartActivity(typeof(RoundActivity));
                    MainActivity.Instance.Finish();
                });
            }
        }

        protected void AssignTeam(Team team,
            ExtendedBinaryReader reader, ExtendedBinaryWriter writer)
        {
            // Send DataSet FileName
            string fn = (!string.IsNullOrEmpty(dataSetFileName)) ?
                dataSetFileName : Event.Current.DataSetFileName;
            writer.Write(fn);

            // Wait to see if we need to send the DataSet over, or if the child already has it
            writer.Write(DataSet.Current.Version);
            bool hasDataSet = reader.ReadBoolean();

            if (!hasDataSet)
            {
                // Send the DataSet in it's entirity
                // TODO: Maybe compress this?
                string dataSetPath = Path.Combine(
                    IO.DataSetDirectory, fn);

                var data = File.ReadAllBytes(dataSetPath);
                writer.Write(data.Length);
                writer.WriteInChunks(data, BluetoothIO.ChunkSize);
            }

            // Send team data to scout
            Print("Assigning Teams...");
            writer.Write(team);
            Print("Done");

            // Update Scout Master UI
            if (ScoutMasterActivity.Instance != null)
            {
                ScoutMasterActivity.UpdateUIFromThread();
            }
        }

        protected void GetRoundData(ExtendedBinaryReader reader)
        {
            // Return if Scout ran into an error
            if (!reader.ReadBoolean())
                return;

            // Get Auto/Tele-OP DataPoints
            var autoPoints = DataSet.Current.RoundAutoData;
            var teleOPPoints = DataSet.Current.RoundTeleOPData;

            // Read Autonomous Data
            var autoData = new object[autoPoints.Count];
            for (int i = 0; i < autoPoints.Count; ++i)
            {
                autoData[i] = reader.ReadByType(
                    autoPoints[i].DataType);
            }

            // Read Tele-OP Data
            var teleOPData = new object[teleOPPoints.Count];
            for (int i = 0; i < teleOPPoints.Count; ++i)
            {
                teleOPData[i] = reader.ReadByType(
                    teleOPPoints[i].DataType);
            }

            // Assign read data to round team slot
            var round = Event.Current.CurrentRound;
            var teamData = round.TeamData[teamIndex];

            teamData.AutoData = autoData;
            teamData.TeleOPData = teleOPData;

            Print($"Set data for {teamIndex} slot of current round");
            Print($"AutoData: {autoData.Length}");
            Print($"TeleOPData: {teleOPData.Length}");
            Print($"Random Auto Data: {autoData[0]}");
        }

        protected void SendRoundData(ExtendedBinaryWriter writer)
        {
            // If we run into an error, let the Scout Master know, then return
            if (RoundActivity.Instance == null)
            {
                writer.Write(false);
                return;
            }

            // Update UI
            MainActivity.SetStatusFromThread(
                "Sending Round Data to Scout Master...");

            // Get Round Auto/Tele-OP Data
            object[] autoData = null, teleOPData = null;
            bool ready = false;

            RoundActivity.Instance.RunOnUiThread(new System.Action(() =>
            {
                autoData = RoundActivity.Instance.AutoData;
                teleOPData = RoundActivity.Instance.TeleOPData;

                RoundActivity.Instance.StartActivity(typeof(MainActivity));
                RoundActivity.Instance.Finish();

                ready = true;
            }));

            // Block until we get needed data from UI thread
            while (!ready) { }

            // Get Auto/Tele-OP DataPoints
            var autoPoints = DataSet.Current.RoundAutoData;
            var teleOPPoints = DataSet.Current.RoundTeleOPData;

            // Let the Scout Master know we're ready
            writer.Write(true);

            // Write Autonomous Data
            for (int i = 0; i < autoPoints.Count; ++i)
            {
                writer.WriteByType(autoData[i],
                    autoPoints[i].DataType);
            }

            // Write Tele-OP Data
            for (int i = 0; i < teleOPPoints.Count; ++i)
            {
                writer.WriteByType(teleOPData[i],
                    teleOPPoints[i].DataType);
            }

            // Update UI again
            MainActivity.ShowFishPunFromThread();
            //MainActivity.SetStatusFromThread(
            //    "Waiting for Scout Master to assign team...");
        }

        protected void GetPreScoutingData(ExtendedBinaryReader reader)
        {
            // Return if Scout ran into an error
            if (!reader.ReadBoolean())
                return;

            // Get Pre-Scouting Data
            var preScoutingPoints = DataSet.Current.PreScoutingData;
            int teamsCount = reader.ReadInt32();
            int teamsAssigned = 0;
            Print($"teamsCount {teamsCount}");

            for (int i = 0; i < teamsCount; ++i)
            {
                var team = reader.ReadTeam();
                team.PreScoutingData = new object[preScoutingPoints.Count];
                Print($"team {team}");

                for (int i2 = 0; i2 < preScoutingPoints.Count; ++i2)
                {
                    team.PreScoutingData[i2] = reader.ReadByType(
                        preScoutingPoints[i2].DataType);
                }

                Print("past data");
                for (int i2 = 0; i2 < Event.Current.Teams.Count; ++i2)
                {
                    var t = Event.Current.Teams[i2];
                    if (t.Name == team.Name && t.ID == team.ID)
                    {
                        t.PreScoutingData = team.PreScoutingData;
                        ++teamsAssigned;
                        Print($"Got Pre-Scouting Data for {team}");
                    }
                }
                Print("past assign loop");
            }

            // Update UI
            if (PreScoutingActivity.Instance == null) return;

            var instance = PreScoutingActivity.Instance;
            instance.RunOnUiThread(new System.Action(() =>
            {
                instance.SetSyncButtonEnabled(true);
                instance.RefreshUI();

                Toast.MakeText(instance, string.Format(
                    "Received Pre-Scouting Data from Scout for {0} team(s).",
                    teamsAssigned), ToastLength.Long).Show();
            }));
        }

        protected void SendPreScoutingData(ExtendedBinaryWriter writer)
        {
            // If we run into an error, let the Scout Master know, then return
            if (Event.Current == null || DataSet.Current == null ||
                DataSet.Current.PreScoutingData == null)
            {
                writer.Write(false);
                return;
            }

            // Get Teams with Pre-Scouting Data
            var teamIndices = new List<int>();
            for (int i = 0; i < Event.Current.Teams.Count; ++i)
            {
                if (Event.Current.Teams[i].PreScoutingData != null)
                    teamIndices.Add(i);
            }

            // Send Pre-Scouting Data
            var preScoutingPoints = DataSet.Current.PreScoutingData;
            writer.Write(true);
            writer.Write(teamIndices.Count);

            foreach (var teamInd in teamIndices)
            {
                var team = Event.Current.Teams[teamInd];
                writer.Write(team);

                for (int i = 0; i < preScoutingPoints.Count; ++i)
                {
                    writer.WriteByType(team.PreScoutingData[i],
                        preScoutingPoints[i].DataType);
                }
            }

            // Update UI
            if (PreScoutingActivity.Instance == null) return;

            var instance = PreScoutingActivity.Instance;
            instance.RunOnUiThread(new System.Action(() =>
            {
                instance.SetSyncButtonEnabled(true);
                instance.RefreshUI();

                Toast.MakeText(instance, "Sent Pre-Scouting Data to Scout Master",
                    ToastLength.Long).Show();
            }));
        }

        protected void GetDatasheet(ExtendedBinaryReader reader)
        {
            // Read Datasheet
            string name = reader.ReadString();
            string data = reader.ReadString();

            // Write Datasheet to file
            string datasheetsDir = Path.Combine(
                IO.ExternalDataDirectory, "DataSheets");
            string pth = Path.Combine(datasheetsDir, name);

            Directory.CreateDirectory(datasheetsDir);
            File.WriteAllText(pth, data);

            // Go to Datasheet UI
            if (MainActivity.Instance != null)
            {
                var inst = MainActivity.Instance;
                BluetoothIO.ConnectThreads.Remove(this);

                inst.RunOnUiThread(() =>
                {
                    var viewIntent = new Intent(inst,
                        typeof(SpreadsheetViewActivity));

                    // TODO: Make it so you can go back to MainActivity.
                    viewIntent.PutExtra("csvData", data);
                    inst.StartActivity(viewIntent);
                    inst.Finish();
                });
            }
        }

        protected void SendDatasheet(ExtendedBinaryWriter writer)
        {
            // Send Datasheet
            writer.Write(ScoutMasterActivity.SyncDatasheetName);
            writer.Write(ScoutMasterActivity.SyncDatasheetData);

            ScoutMasterActivity.SyncDatasheetName =
                ScoutMasterActivity.SyncDatasheetData = null;

            // Update UI
            if (ScoutMasterActivity.Instance == null) return;
            var instance = ScoutMasterActivity.Instance;

            instance.RunOnUiThread(new System.Action(() =>
            {
                instance.SetSyncButtonEnabled(true);
                Toast.MakeText(instance, "Sent Datasheet!",
                    ToastLength.Long).Show();
            }));
        }

        protected void Print(string txt)
        {
            // TODO: REMOVE THIS DEBUG METHOD
            Log.Debug("MyScout", txt);
        }
    }
}