﻿namespace MyScout.Android
{
    public class Team
    {
        // Variables/Constants
        public object[] PreScoutingData;
        public string Name, ID;

        // Constructors
        public Team(string name)
        {
            Name = name;
        }

        public Team(string name, string id)
        {
            Name = name;
            ID = id;
        }

        // Methods
        public override string ToString()
        {
            // Return "Name  - ID" only if this team has both.
            // Otherwise, return just the Name or the ID.
            return (string.IsNullOrEmpty(Name)) ?
                ID : ((string.IsNullOrEmpty(ID)) ?
                Name : $"{Name} - {ID}");
        }
    }
}