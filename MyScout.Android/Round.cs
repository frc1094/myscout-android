﻿namespace MyScout.Android
{
    public class Round
    {
        // Variables/Constants
        /// <summary>
        /// First 3 Teams are on the red alliance.
        /// The other 3 teams are on the blue alliance
        /// </summary>
        public TeamData[] TeamData = new TeamData[TeamCount];
        public const int TeamCount = 6;
    }

    public class TeamData
    {
        // Variables/Constants
        public object[] AutoData;
        public object[] TeleOPData;
        public int TeamIndex;

        public Team Team
        {
            get
            {
                if (TeamIndex < 0 || Event.Current == null ||
                    Event.Current.Teams.Count <= TeamIndex)
                    return null;

                return Event.Current.Teams[TeamIndex];
            }

            set
            {
                if (Event.Current == null)
                    return;

                TeamIndex = Event.Current.Teams.IndexOf(value);
            }
        }

        // Constructors
        public TeamData(int teamIndex)
        {
            TeamIndex = teamIndex;
        }
    }
}