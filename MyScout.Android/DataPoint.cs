﻿using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Text;
using Android.Widget;
using MyScout.Android.UI;
using System;
using System.Collections.Generic;
using System.IO;
using static Android.App.ActionBar;

namespace MyScout.Android
{
    public class DataPoint
    {
        // Variables/Constants
        public Dictionary<string, Choice> Choices =
            new Dictionary<string, Choice>();

        public string Name;
        public Type DataType;
        public int Weight = -1;

        // Constructors
        public DataPoint() { }
        public DataPoint(string name, Type dataType, int weight = -1,
            Dictionary<string, Choice> choices = null)
        {
            Name = name;
            DataType = dataType;
            Weight = weight;

            if (choices != null)
                Choices = choices;
        }

        // Methods
        public void AddGUIWidget(Context context, LinearLayout layout)
        {
            // Add Label
            var label = new TextView(context)
            {
                Text = Name,
            };

            label.SetTextAppearance(context,
                Resource.Style.TextAppearance_AppCompat_Medium);

            var layoutParams = new LinearLayout.LayoutParams(
                LayoutParams.MatchParent, LayoutParams.WrapContent);

            layoutParams.SetMargins(0, 5, 0, 0);
            label.SetTextColor(Color.White);
            layout.AddView(label, layoutParams);

            // Multiple Choices
            if (Choices.Count > 0)
            {
                var group = new RadioGroup(context);
                bool checkedFirst = false;

                foreach (var choice in Choices)
                {
                    var rb = new RadioButton(context)
                    {
                        Text = choice.Key,
                        Tag = choice.Value
                    };

                    group.AddView(rb);
                    if (!checkedFirst)
                    {
                        group.Check(rb.Id);
                        checkedFirst = true;
                    }
                }

                layout.AddView(group);
                return;
            }

            // Add Widget
            if (DataType == typeof(bool))
            {
                layout.AddView(new CheckBox(context)
                {
                    Text = "",
                    //ScaleX = 2,
                    //ScaleY = 2
                });
            }
            else if (DataType == typeof(int))
            {
                var l = new RelativeLayout(context);
                var view = new TextView(context)
                {
                    Text = "0",
                };

                view.SetTextAppearance(context,
                    Resource.Style.TextAppearance_AppCompat_Medium);

                view.SetTypeface(view.Typeface, TypefaceStyle.Bold);
                view.SetTextColor(Color.White);

                // Minus Button
                var minusBtn = new Button(context)
                {
                    Text = "-",
                    Tag = view
                };

                var lParams = new RelativeLayout.LayoutParams(
                    LayoutParams.WrapContent, LayoutParams.WrapContent);

                minusBtn.Click += MinusBtn_Click;
                lParams.AddRule(LayoutRules.AlignParentLeft);
                l.AddView(minusBtn, lParams);

                // Text
                lParams = new RelativeLayout.LayoutParams(
                    LayoutParams.WrapContent, LayoutParams.WrapContent);

                lParams.AddRule(LayoutRules.CenterInParent);
                l.AddView(view, lParams);

                // Plus Button
                var plusBtn = new Button(context)
                {
                    Text = "+",
                    Tag = view
                };

                lParams = new RelativeLayout.LayoutParams(
                    LayoutParams.WrapContent, LayoutParams.WrapContent);

                plusBtn.Click += PlusBtn_Click;
                lParams.AddRule(LayoutRules.AlignParentRight);
                l.AddView(plusBtn, lParams);

                // Layout
                layoutParams = new LinearLayout.LayoutParams(
                    LayoutParams.WrapContent, LayoutParams.WrapContent);

                layoutParams.SetMargins(0, 0, 25, 0);
                layout.AddView(l, layoutParams);
            }
            else if (DataType == typeof(Time) || DataType == typeof(JavaList<Time>))
            {
                var l = new RelativeLayout(context);
                var btn = new Button(context)
                {
                    Text = "Start Timer",
                };

                if (DataType == typeof(JavaList<Time>))
                    btn.Tag = new JavaList<Time>();
                else
                    btn.Tag = new Time();

                var lParams = new RelativeLayout.LayoutParams(
                    LayoutParams.MatchParent, LayoutParams.WrapContent);

                btn.Click += TimerBtn_Click;
                lParams.AddRule(LayoutRules.CenterInParent);
                l.AddView(btn, lParams);

                // Layout
                layoutParams = new LinearLayout.LayoutParams(
                    LayoutParams.WrapContent, LayoutParams.WrapContent);

                layoutParams.SetMargins(0, 0, 25, 0);
                layout.AddView(l, layoutParams);
            }
            else
            {
                bool isNumber = (DataType == typeof(int) ||
                    DataType == typeof(float) || DataType == typeof(double));

                bool isDecimal = (isNumber &&
                    (DataType == typeof(float) || DataType == typeof(double)));

                // TODO: Look into more touch-screen-friendly ways of doing number boxes
                var textBx = new EditText(context)
                {
                    Hint = Name,
                    InputType = (!isNumber) ? InputTypes.ClassText : InputTypes.ClassNumber |
                        ((!isDecimal) ? InputTypes.NumberFlagSigned :
                        InputTypes.NumberFlagDecimal | InputTypes.NumberFlagSigned)
                };

                if (isNumber)
                {
                    // Set filters to limit maximum values that can be given
                    textBx.SetFilters(new IInputFilter[] {
                        new NumericTypeRangeFilter(DataType) });
                }

                layout.AddView(textBx);
            }
        }

        public void WriteCSVColumn(StreamWriter writer)
        {
            if (DataType == typeof(int) || DataType == typeof(float) ||
                DataType == typeof(double) || DataType == typeof(JavaList<Time>))
            {
                writer.Write($",Total {Name}");
                writer.Write($",Avg {Name}");
            }
            else
            {
                writer.Write($",{Name}");
            }
        }

        public string GetDisplayValue(object data)
        {
            if (Choices.Count > 0)
            {
                foreach (var choice in Choices)
                {
                    if (choice.Value.Object.Equals(data))
                        return choice.Key;
                }
            }

            return data.ToString();
        }

        // GUI Events
        private void MinusBtn_Click(object sender, EventArgs e)
        {
            var btn = (sender as Button);
            if (btn == null || btn.Tag == null)
                return;

            var numTxt = (btn.Tag as TextView);
            if (numTxt == null || !int.TryParse(numTxt.Text, out int i))
                return;

            if (--i < 0)
                return;

            numTxt.Text = i.ToString();
        }

        private void PlusBtn_Click(object sender, EventArgs e)
        {
            var btn = (sender as Button);
            if (btn == null || btn.Tag == null)
                return;

            var numTxt = (btn.Tag as TextView);
            if (numTxt == null || !int.TryParse(numTxt.Text, out int i))
                return;

            numTxt.Text = (++i).ToString();
        }

        private void TimerBtn_Click(object sender, EventArgs e)
        {
            var btn = (sender as Button);
            if (btn == null || btn.Tag == null)
                return;

            var time = (btn.Tag as Time);
            bool doStopTimer = (btn.Text == "Stop Timer");

            // If statement for if this is a list of times, rather than a single time.
            if (time == null)
            {
                var times = (btn.Tag as JavaList<Time>);
                if (times == null) return;

                // Make a new Time if "Start Timer" was hit
                if (!doStopTimer)
                    times.Add(new Time());

                time = times[times.Count - 1];

                // Set text differently if "Stop Timer" was hit
                if (doStopTimer)
                {
                    time.StopTiming();
                    btn.Text = $"Start New Timer ({times.Count})";
                    return;
                }
            }

            // Start or Stop the Timer
            if (doStopTimer)
            {
                time.StopTiming();
                btn.Text = $"Reset Timer ({time.Duration.Seconds} Seconds)";
            }
            else
            {
                time.StartTiming();
                btn.Text = "Stop Timer";
            }
        }

        // Other
        public class Choice : Java.Lang.Object
        {
            // Variables/Constants
            public object Object { get; protected set; }

            // Constructors
            public Choice(object obj)
            {
                Object = obj;
            }
        }
    }
}