﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MyScout.Android
{
    public class Event
    {
        // Variables/Constants
        public static Event Current;
        public List<Team> Teams = new List<Team>();
        public List<Round> Rounds = new List<Round>();
        public string Name, DataSetFileName;

        public int CurrentRoundIndex = -1, Index = -1;
        public Round CurrentRound
        {
            get
            {
                if (CurrentRoundIndex < 0 || CurrentRoundIndex >= Rounds.Count)
                    return null;

                return Rounds[CurrentRoundIndex];
            }
        }

        public static string EventsDirectory
        {
            get => Path.Combine(
                IO.ExternalDataDirectory, EventsDirName);
        }
        
        public const string EventsDirName = "Events",
            FileNamePrefix = "Event", Extension = ".event";

        protected const string Signature = "EVN";
        protected const byte Version = 2; // The highest-supported version of the format

        // Methods
        public static Event GetEventEntry(string filePath)
        {
            using (var fs = File.OpenRead(filePath))
            {
                return GetEventEntry(fs);
            }
        }

        public static Event GetEventEntry(Stream fileStream)
        {
            // Read event header data
            var evnt = new Event();
            return (evnt.LoadHeaderData(fileStream)) ?
                evnt : null;
        }

        public override string ToString()
        {
            return Name;
        }

        public void ImportTeamsCSV(string filePath)
        {
            using (var fs = File.OpenRead(filePath))
            {
                ImportTeamsCSV(fs);
            }
        }

        public void ImportTeamsCSV(Stream fileStream)
        {
            using (var reader = new StreamReader(fileStream))
            {
				bool isFirstLine = true, addTeam = true;
                string line;

                while (!string.IsNullOrEmpty((line = reader.ReadLine())))
                {
                    var columns = line.Split(',');
					if (columns == null || (columns.Length < 2 &&
						!isFirstLine) || columns.Length < 1 ||
						!int.TryParse(columns[0], out int teamID))
					{
						isFirstLine = false;
						continue;
					}

					addTeam = true;
                    foreach (var team in Teams)
                    {
                        // Update teams with duplicate IDs or Names
                        if (team.ID == columns[0] || team.Name == columns[1])
                        {
                            team.ID = columns[0];
                            team.Name = columns[1];
							addTeam = false;
							break;
                        }
                    }

					if (addTeam)
					{
						Teams.Add(new Team(columns[0], columns[1]));
					}

					isFirstLine = false;
                }
            }
        }

        public void ImportRoundsCSV(string filePath)
        {
            using (var fs = File.OpenRead(filePath))
            {
                ImportRoundsCSV(fs);
            }
        }

        public void ImportRoundsCSV(Stream fileStream)
        {
            using (var reader = new StreamReader(fileStream))
            {
				bool isFirstLine = true;
                string line;

                while (!string.IsNullOrEmpty((line = reader.ReadLine())))
                {
                    var columns = line.Split(',');
                    if (columns == null || (columns.Length < 7 && !isFirstLine) ||
						columns.Length < 1 || !int.TryParse(columns[0],
						out int matchNumber))
                    {
						isFirstLine = false;
						continue;
                    }

                    var round = new Round();
                    AddTeam(0, 4);
                    AddTeam(1, 5);
                    AddTeam(2, 6);
                    AddTeam(3, 1);
                    AddTeam(4, 2);
                    AddTeam(5, 3);

                    --matchNumber;
                    if (matchNumber == Rounds.Count)
                    {
                        Rounds.Add(round);
                    }
                    else if (matchNumber < Rounds.Count)
                    {
                        Rounds[matchNumber] = round;
                    }

					isFirstLine = false;

                    // Sub-Methods
                    void AddTeam(int teamIndex, int columnIndex)
                    {
                        Team team;
                        var column = columns[columnIndex];

                        for (int i = 0; i < Teams.Count; ++i)
                        {
                            team = Teams[i];
                            if (team.ID == column || column.Contains(team.ID) ||
                                column.Contains(team.Name))
                            {
                                round.TeamData[teamIndex] = new TeamData(i);
                                return;
                            }
                        }
                    }
                }
            }
        }

        public void ExportCSV(string filePath)
        {
            using (var fs = File.Create(filePath))
            {
                ExportCSV(fs);
            }
        }

        public void ExportCSV(Stream fileStream)
        {
            using (var writer = new StreamWriter(fileStream, new UTF8Encoding(false)))
            {
                DataSet.Current.WriteDataSheetColumns(writer, false);

                // Rows
                for (int i = 0; i < Teams.Count; ++i)
                {
                    // Write Team ID
                    writer.Write(Teams[i].ID);

                    // Generate Round Data
                    var totals = DataSet.Current.ComputeTotals(
                        Rounds, i, out int[] pointCounts);

                    var averages = DataSet.ComputeAverages(
                        totals, pointCounts);

                    // Write Round Data
                    DataSet.Current.WriteDataSheetRow(writer,
                        totals, averages);

                    writer.WriteLine();
                }
            }
        }

        public void ExportPreScoutingCSV(string filePath)
        {
            using (var fs = File.Create(filePath))
            {
                ExportPreScoutingCSV(fs);
            }
        }

        public void ExportPreScoutingCSV(Stream fileStream)
        {
            var writer = new StreamWriter(fileStream, new UTF8Encoding(false));
            int c = DataSet.Current.PreScoutingData.Count;
            DataSet.Current.WriteDataSheetColumns(writer, true);

            // Rows
            foreach (var team in Teams)
            {
                // Write Team ID
                writer.Write(team.ID);

                // Write Pre-Scouting Data
                for (int i = 0; i < c; ++i)
                {
                    if (team.PreScoutingData == null || c > team.PreScoutingData.Length)
                        writer.Write(", ");
                    else
                    {
                        var point = DataSet.Current.PreScoutingData[i];
                        string txt = point.GetDisplayValue(team.PreScoutingData[i]);
                        writer.Write($",{txt}");
                    }
                }

                writer.WriteLine();
            }

            writer.Close();
        }

        public void Load(int index)
        {
            // Get the event's file path
            Index = index;
            string filePath = Path.Combine(EventsDirectory,
                $"{FileNamePrefix}{index}{Extension}");

            // Load the event
            Load(filePath);
        }

        public void Load(string filePath)
        {
            using (var fs = File.OpenRead(filePath))
            {
                Load(fs);
            }
        }

        public void Load(Stream fileStream)
        {
            // Header
            var reader = new ExtendedBinaryReader(
                fileStream, Encoding.UTF8, true);

            if (!LoadHeaderData(reader, out byte ver))
                return;

            // Teams
            int teamCount = reader.ReadInt32();
            Teams.Clear();

            var preScoutingPoints = DataSet.Current.PreScoutingData;
            for (int i = 0; i < teamCount; ++i)
            {
                var team = reader.ReadTeam();

                // Pre-Scouting Data
                if (ver > 1)
                {
                    bool hasPreScouting = reader.ReadBoolean();
                    if (hasPreScouting)
                    {
                        if (preScoutingPoints == null)
                        {
                            throw new Exception(string.Format(
                                "Cannot read event; DataSet does {0} ",
                                "not contain pre-scouting DataPoints."));
                        }

                        var preScoutingData = new object[preScoutingPoints.Count];
                        for (int i2 = 0; i2 < preScoutingData.Length; ++i2)
                        {
                            preScoutingData[i2] = reader.ReadByType(
                                preScoutingPoints[i2].DataType);
                        }

                        team.PreScoutingData = preScoutingData;
                    }
                }

                Teams.Add(team);
            }

            // Rounds
            int roundCount = reader.ReadInt32();
            CurrentRoundIndex = reader.ReadInt32();

            Rounds.Clear();
            if (CurrentRoundIndex >= roundCount || CurrentRoundIndex < 0)
                CurrentRoundIndex = (roundCount - 1);

            for (int i = 0; i < roundCount; ++i)
            {
                Rounds.Add(ReadRoundData(reader, DataSet.Current));
            }

            reader.Close();
        }

        protected Round ReadRoundData(ExtendedBinaryReader reader, DataSet dataSet)
        {
            // Read per-round data for each team
            var round = new Round();
            for (int i = 0; i < Round.TeamCount; ++i)
            {
                // Read Team Index
                int teamIndex = reader.ReadInt32();
                if (teamIndex < 0) continue;

                var teamData = new TeamData(teamIndex);

                // Read Autonomous Data
                if (reader.ReadBoolean())
                {
                    teamData.AutoData = new object[dataSet.RoundAutoData.Count];
                    for (int i2 = 0; i2 < dataSet.RoundAutoData.Count; ++i2)
                    {
                        teamData.AutoData[i2] = reader.ReadByType(
                            dataSet.RoundAutoData[i2].DataType);
                    }
                }

                // Read Tele-OP Data
                if (reader.ReadBoolean())
                {
                    teamData.TeleOPData = new object[dataSet.RoundTeleOPData.Count];
                    for (int i2 = 0; i2 < dataSet.RoundTeleOPData.Count; ++i2)
                    {
                        teamData.TeleOPData[i2] = reader.ReadByType(
                            dataSet.RoundTeleOPData[i2].DataType);
                    }
                }

                round.TeamData[i] = teamData;
            }

            return round;
        }

        public void Save()
        {
            // Create the Events directory if it does not exist
            string eventsDir = EventsDirectory; // This is so we don't keep calling the getter
            Directory.CreateDirectory(eventsDir);

            // Get the event's file path
            string filePath = Path.Combine(
                eventsDir, $"{FileNamePrefix}{Index}{Extension}");

            if (Index <= 0)
            {
                Index = 0;
                do
                {
                    filePath = Path.Combine(eventsDir,
                        $"{FileNamePrefix}{++Index}{Extension}");
                }
                while (File.Exists(filePath));
            }

            // Make a backup of the event if necessary
            if (File.Exists(filePath))
            {
                string backupDir = Path.Combine(
                    IO.ExternalDataDirectory, "Backup");

                string backupPath = Path.Combine(backupDir,
                    $"{FileNamePrefix}{Index}{Extension}");

                Directory.CreateDirectory(backupDir);
                File.Copy(filePath, backupPath, true);
            }

            // Save the event
            Save(filePath);
        }

        public void Save(string filePath)
        {
            using (var fs = File.Create(filePath))
            {
                Save(fs);
            }
        }

        public void Save(Stream fileStream)
        {
            // Header
            var writer = new ExtendedBinaryWriter(
                fileStream, Encoding.UTF8, true);

            writer.WriteSignature(Signature);
            writer.Write(Version);
            writer.Write(Name);
            writer.Write(DataSetFileName);

            // Teams
            var preScoutingDataPoints = DataSet.Current?.PreScoutingData;
            writer.Write(Teams.Count);

            foreach (var team in Teams)
            {
                writer.Write(team);

                // Pre-Scouting Data
                if (preScoutingDataPoints == null || team.PreScoutingData == null)
                {
                    writer.Write(false);
                }
                else
                {
                    writer.Write(true);
                    for (int i = 0; i < preScoutingDataPoints.Count; ++i)
                    {
                        writer.WriteByType(team.PreScoutingData[i],
                            preScoutingDataPoints[i].DataType);
                    }
                }
            }

            // Rounds
            writer.Write(Rounds.Count);
            writer.Write(CurrentRoundIndex);
            
            foreach (var round in Rounds)
            {
                WriteRoundData(writer, DataSet.Current, round);
            }

            writer.Close();
        }

        protected void WriteRoundData(ExtendedBinaryWriter writer,
            DataSet dataSet, Round round)
        {
            // Write per-round data for each team
            for (int i = 0; i < Round.TeamCount; ++i)
            {
                if (i >= round.TeamData.Length || round.TeamData[i] == null)
                {
                    writer.Write(-1);
                    continue;
                }

                // Write Team Index
                var teamData = round.TeamData[i];
                writer.Write(teamData.TeamIndex);

                // Write Autonomous Data
                bool hasAutoData = (teamData.AutoData != null);
                writer.Write(hasAutoData);

                if (hasAutoData)
                {
                    for (int i2 = 0; i2 < teamData.AutoData.Length; ++i2)
                    {
                        writer.WriteByType(teamData.AutoData[i2],
                            dataSet.RoundAutoData[i2].DataType);
                    }
                }

                // Write Tele-OP Data
                bool hasTeleOPData = (teamData.TeleOPData != null);
                writer.Write(hasTeleOPData);

                if (hasTeleOPData)
                {
                    for (int i2 = 0; i2 < teamData.TeleOPData.Length; ++i2)
                    {
                        writer.WriteByType(teamData.TeleOPData[i2],
                            dataSet.RoundTeleOPData[i2].DataType);
                    }
                }
            }
        }

        protected bool LoadHeaderData(Stream fileStream)
        {
            var reader = new ExtendedBinaryReader(
                fileStream, Encoding.UTF8, true);

            return LoadHeaderData(reader, out byte version);
        }

        protected bool LoadHeaderData(ExtendedBinaryReader reader,
            out byte version)
        {
            if (reader.ReadSignature() != Signature)
            {
                version = 0;
                return false;
            }

            version = reader.ReadByte();
            if (version > Version)
                return false;

            Name = reader.ReadString();
            DataSetFileName = reader.ReadString();
            return true;
        }
    }
}