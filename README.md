# 1094 MyScout (Android)
An Android application developed by [FRC team 1094 "The Channel Cats"](http://frc1094.org/)
for easy and powerful collection and manipulation of robot field performance data.

## DataSets
DataSets are XML files which specify the type of data that needs to be collected by the scouts for this game.
Everything from the UI, to the data stored within events, to data exported to CSV files is based on the values
contained within the DataSet, and therefore can be modified easily simply by editing the DataSet XML file.

## Bundled DataSets
A Bundled DataSet is a DataSet that comes bundled with MyScout itself and gets copied over to the DataSets directory automatically when needed.
MyScout comes with the following DataSets bundled:

- 2017Game.xml
- 2018Game.xml